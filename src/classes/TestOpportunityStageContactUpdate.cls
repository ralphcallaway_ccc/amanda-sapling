@IsTest
private class TestOpportunityStageContactUpdate
{
    private static TestMethod void testTrigger(){
        
        //Step 1 : Data Insertion
        Account a=new Account(Name='Test Account',BillingCountry='Peru',ShippingCountry='Peru');
           insert a;
        Opportunity o = new Opportunity(Name='Test Opportunity',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.10, Textbook_Author_Title__c='Other' );
        insert o;  
        Contact c = new Contact(FirstName='John',LastName='Doe',Prior_Sapling_User__c=False, Prior_Demo__c=True);
        insert c;
        Opportunity_Contact_Role__c cocr = new Opportunity_Contact_Role__c (Opportunity__c = o.id, Contact__c=c.id, role__c='Decision Maker', Primary__c=True) ;     
        insert cocr;
        
        test.startTest();

        o.StageName = 'Left VM/E-Mail/Conversation';
        update o;
        

        cocr = [SELECT Id, Opportunity__c, Opportunity_Stage__c FROM Opportunity_Contact_Role__c WHERE opportunity__c=:o.Id];
        system.assertEquals('Left VM/E-Mail/Conversation', cocr.Opportunity_Stage__c);
        

        test.stopTest();
    }
}