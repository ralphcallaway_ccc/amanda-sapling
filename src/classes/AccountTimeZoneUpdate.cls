@IsTest
private class AccountTimeZoneUpdate
{
    private static TestMethod void testTrigger(){
    test.startTest();        
        //Step 1 : Data Insertion
       Country__c c=new Country__c (Name='Peru', Timezone__c='GMT-05:00');
           insert c;
        Account a=new Account(Name='Test Account', BillingCountry='Peru');
           insert a;  
        Account a1=new Account(Name='Test USNC', BillingCountry='United States', BillingState='NC');
           insert a1;
        Account a2=new Account(Name='Test USTN', BillingCountry='United States', BillingState='TN');
           insert a2;
        Account a3=new Account(Name='Test USUT', BillingCountry='United States', BillingState='UT');
           insert a3;
        Account a4=new Account(Name='Test USCA', BillingCountry='United States', BillingState='CA');
           insert a4;
        Account a5=new Account(Name='Test USAZ', BillingCountry='United States', BillingState='AZ');
           insert a5;
        Account a6=new Account(Name='Test USAK', BillingCountry='United States', BillingState='AK');
           insert a6;
        Account a7=new Account(Name='Test USHI', BillingCountry='United States', BillingState='HI');
           insert a7;
       
    
        //reload objects to make sure values are loaded
        c = [select id, name, Timezone__c from country__c where id=:c.id];
        a= [select id, billingcountry, Timezone__c from Account where id=:a.id];
        a1= [select id, billingcountry, Timezone__c from Account where id=:a1.id];
        a2= [select id, billingcountry, Timezone__c from Account where id=:a2.id];
        a3= [select id, billingcountry, Timezone__c from Account where id=:a3.id];
        a4= [select id, billingcountry, Timezone__c from Account where id=:a4.id];
        a5= [select id, billingcountry, Timezone__c from Account where id=:a5.id];
        a6= [select id, billingcountry, Timezone__c from Account where id=:a6.id];
        a7= [select id, billingcountry, Timezone__c from Account where id=:a7.id];
        
               
       
        //assert your results using system.assert and system.asserEquals
        system.assert(a.Timezone__c == c.Timezone__c);            
        
        
     test.stopTest();
    }
}