public with sharing class GlobalHelper {

    /// <summary>
    /// OVERLOADED
    /// CHECKS IF STRING IS NULL OR EMPTY
    /// </summary>
    /// <param name="sInValueToCheck">STRING TO CHECK</param>
    /// <returns>TRUE IF NOT NULL</returns>
    public static boolean CheckForNotNull(string sInValueToCheck)
    {
          if(sInValueToCheck != null && sInValueToCheck != '' && sInValueToCheck.toLowerCase() != 'null')
          {
                return true;
          }
          return false;
    }
    /// <summary>
    /// OVERLOADED
    /// CHECKS IF SOBJECT LIST IS NULL OR EMPTY
    /// </summary>
    /// <param name="lstInValueToCheck">STRING TO CHECK</param>
    /// <returns>TRUE IF NOT NULL</returns>
    public static boolean CheckForNotNull(List<sobject> lstInValueToCheck)
    {
          if(lstInValueToCheck != null && lstInValueToCheck.size() > 0)
          {
                return true;
          }
          return false;
    }
    
    /// <summary>
    /// GETS THE PARAM VALUE BASED ON KEY FROM URL
    /// </summary>
    /// <param name="sInUrlKey">URL PARAM KEY</param>
    /// <returns>PARAM VALUE TO LOWER</returns>
    public static string UrlParamToLower(string sInUrlKey)
    {
          if(GlobalHelper.CheckForNotNull(sInUrlKey))
          {
                String UrlParam = ApexPages.currentPage().getParameters().get(sInUrlKey);
                if(GlobalHelper.CheckForNotNull(UrlParam))
                {
                      UrlParam = UrlParam.toLowerCase().trim();
                }
                return UrlParam;
          }
          return null;
    }
    
    /// <summary>
    /// OVERLOADED
    /// GETS THE PARAM VALUE BASED ON KEY FROM URL
    /// </summary>
    /// <param name="sInUrlKey">URL PARAM KEY</param>
    /// <returns>PARAM VALUE</returns>
    public static string UrlParam(string sInUrlKey)
    {
          if(GlobalHelper.CheckForNotNull(sInUrlKey))
          {
                return ApexPages.currentPage().getParameters().get(sInUrlKey);
          }
          return null;
    }
       
    private static Map<integer, string> hexMap = new Map<integer, string>();
        static {
        hexMap.put(0, '0');
        hexMap.put(1, '1');
        hexMap.put(2, '2');
        hexMap.put(3, '3');
        hexMap.put(4, '4');
        hexMap.put(5, '5');
        hexMap.put(6, '6');
        hexMap.put(7, '7');
        hexMap.put(8, '8');
        hexMap.put(9, '9');
        hexMap.put(10, 'A');
        hexMap.put(11, 'B');
        hexMap.put(12, 'C');
        hexMap.put(13, 'D');
        hexMap.put(14, 'E');
        hexMap.put(15, 'F');
    }
    
    public static string IntToHexString(Integer InputInt, Integer CharLength)
    {
        String HexString = '';
        Integer Dec = InputInt;
        While (Dec>0) 
        {
            HexString = (hexmap.get(math.mod(Dec, 16)))+ HexString;
            Dec = Dec/16;
        }
        
        Integer Diff = CharLength - HexString.length();
        While (Diff > 0)
        {
            HexString = '0'+HexString;
            Diff = Diff -1;
        }
        
        Return HexString;
    }

    public static string LongToHexString(Long InputLong, Integer CharLength)
    {
        String HexString = '';
        Long Dec = InputLong;
        While (Dec>0) 
        {
            HexString = (hexmap.get(Integer.valueof(math.mod(Dec, 16))))+ HexString;
            Dec = Dec/16;
        }

        Integer Diff = CharLength - HexString.length();
        While (Diff > 0)
        {
            HexString = '0'+HexString;
            Diff = Diff -1;
        }
        
        Return HexString;
    }
    
    public static string DecToHexString(Decimal InputDec, Integer CharLength)
    {
        String HexString = '';
        Integer Dec = Integer.valueOf(InputDec);
        While (Dec>0) 
        {
            HexString = (hexmap.get(math.mod(Dec, 16)))+ HexString;
            Dec = Dec/16;
        }

        Integer Diff = CharLength - HexString.length();
        While (Diff > 0)
        {
            HexString = '0'+HexString;
            Diff = Diff -1;
        }
        
        Return HexString;
    }
 
}