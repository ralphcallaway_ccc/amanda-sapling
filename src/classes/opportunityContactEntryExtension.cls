public with sharing class opportunityContactEntryExtension {

    public Opportunity theOpp {get;set;}
    public String searchString {get;set;}
    public Opportunity_Contact_Role__c[] chosencontacts {get;set;}
    public contact[] availablecontacts {get;set;}
    public Account theAccount {get;set;}   
    
    public String toSelect {get; set;}
    public String toUnselect {get; set;}
    public Decimal Total {get;set;}
    
    public Boolean overLimit {get;set;}
    
    private Opportunity_Contact_Role__c[] forDeletion = new Opportunity_Contact_Role__c[]{};


    public opportunityContactEntryExtension(ApexPages.StandardsetController controller) {

        Id opptyId = ApexPages.currentPage().getParameters().get('id');

        // Get information about the Opportunity being worked on
        theOpp = [select Id, AccountId, Account.Name from Opportunity where Id = :opptyId limit 1];
        
        // If products were previously selected need to put them in the "selected products" section to start with
        chosencontacts = [
            select Id, Contact__c, Contact_Name__c, Opportunity__c, Account_Manager_Notes__c, Date_Demo_Completed__c, 
                Delay_Followup_Until__c, Demo_Status__c, Demo_Time_zone__c, Demo_Tracked_On__c, PreDemo_Info_Quality__c, 
                Primary__c, Role__c, Sales_Scientist_Notes__c, Time_Demo_Requested__c 
            from Opportunity_Contact_Role__c 
            where Opportunity__c = :theOpp.Id
        ];

        //show associated account
        theAccount = theOpp.Account;
        updateAvailableList();
        
        
    }
    
    // this is the 'action' method on the page
    public PageReference AccountCheck(){
        //if there is only one Account we go with it and save the opp
            if(theOpp.AccountId != theAccount.Id){
                try{
                    theOpp.AccountId = theAccount.Id;
                    update(theOpp);
                }
                catch(Exception e){
                    ApexPages.addMessages(e);
                }
            }
            
            return null;
    
    }
       
    
    public void updateAvailableList() {
    
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'select Id, AccountId, name, firstname, lastname, email, phone from contact where AccountId = \'' + theAccount.Id + '\'';
                
        // note that we are looking for the search string entered by the user in the First Name OR Last Name
        // modify this to search other fields if desired
        if(searchString!=null){
            qString+= ' and (contact.FirstName like \'%' + searchString + '%\' or contact.LastName like \'%' + searchString + '%\')';
        }
        
        Set<Id> selectedEntries = new Set<Id>();
        for(Opportunity_Contact_Role__c d:chosencontacts){
            selectedEntries.add(d.contact__c);
        }
        
        if(selectedEntries.size()>0){
            String tempFilter = ' and Id not in (';
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            qString+= extraFilter;
        }
        
        qString+= ' order by Contact.Name';
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        availablecontacts = database.query(qString);
        
        // We only display up to 100 results... if there are more than we let the user know (see vf page)
        if(availablecontacts.size()==101){
            availablecontacts.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
    }
    
    public void addTochosencontacts(){
    
        // This function runs when a user hits "select" button next to a contact
    
        for(contact d : availablecontacts){
            if((String)d.Id==toSelect){
                chosencontacts.add(new Opportunity_Contact_Role__c(Opportunity__c=theOpp.Id, contact__c=d.id, role__c='Decision Maker'));
                break;
            }
        }
        
        updateAvailableList();  
    }
    

    public PageReference removeFromchosencontacts(){
    
        // This function runs when a user hits "remove" on an item in the "Chosen Contacts" section
    
        Integer count = 0;
    
        for(Opportunity_Contact_Role__c d : chosencontacts){
            if((String)d.contact__c==toUnselect){
            
                if(d.Id!=null)
                    forDeletion.add(d);
            
                chosencontacts.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }
    
    public PageReference onSave(){
    
        // If previously selected contacts are now removed, we need to delete them
        if(forDeletion.size()>0)
            delete(forDeletion);
    
        // Previously selected contacts may have new quantities and amounts, and we may have new products listed, so we use upsert here
 
			if (chosencontacts.size() > 0)
			{
				// go through choosenContacts and check if any has primary__c set to true
				Boolean primaryFound = false;
				for (Opportunity_Contact_Role__c ocr:chosencontacts)

				{
				  if (ocr.Primary__c == true) {
					primaryFound = true;
				  }
                                system.debug('###PRIMARY Status ' + primaryFound + ocr);  
				}
system.debug('###PRIMARY FOUND ' + primaryFound);
				// if there's a primary upsert em
				
				if (PrimaryFound == True){
system.debug('###PRIMARY FOUND ' + primaryFound);
					upsert(chosencontacts);
				}
				// otherwise show an error
				else {
system.debug('###PRIMARY NOT FOUND ' + primaryFound);
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Select a Primary Contact'));
					return null;
				}
			}
			
			
			
	           
        // After save return the user to the Opportunity
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Opportunity   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
 
}