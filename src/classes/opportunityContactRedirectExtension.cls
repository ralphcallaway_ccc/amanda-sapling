public with sharing class opportunityContactRedirectExtension {

    Id oppId;

    // we are extending the Custom Opportunity Contact Role controller, so we query to get the parent OpportunityId
    public opportunityContactRedirectExtension(ApexPages.StandardController controller) {
        oppId = [select Id, Opportunity__c from Opportunity_Contact_Role__c where Id = :controller.getRecord().Id limit 1].Opportunity__c;
    }
    
    // then we redirect to our desired page with the Opportunity Id in the URL
    public pageReference redirect(){
        return new PageReference('/apex/opportunityContactEntry?id=' + oppId);
    }

}