public class NewDemofromOpportunityExtension {
    
     //added an instance variable for the standard controller
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Opportunity opp {get;set;}
    // set the id of the record that is created -- ONLY USED BY THE TEST CLASS
    public ID newRecordId {get;set;}
 
    // initialize the controller
    public NewDemofromOpportunityExtension(ApexPages.StandardController controller) {
 
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        opp = (Opportunity)controller.getRecord();
       } 
         // method called from the VF's action attribute to create the New Demo
    public PageReference NewDemo() {
 
         // setup the save point for rollback
         Savepoint sp = Database.setSavepoint();
         Demo__c newDemo;
         Demo_Opportunity__c newDemoOPP;
         Demo_Contact__c newDemoCon;
 
         try {
 
              //get information from opp to go to Demo - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
             opp = [select Id, OwnerID, StageName,Partner_Generated_Lead_to_Sapling__c, Competitor__c, Account.Academic_Calendar__c, Name from Opportunity where id = :opp.id];
             newDemo = New Demo__c();
             newDemo.Competitors_in_Consideration__c = opp.Competitor__c;
             newDemo.Partner_Lead_Source__c = opp.Partner_Generated_Lead_to_Sapling__c;
             newDemo.AM_RM__c = opp.OwnerID;
             newDemo.Academic_Calendar__c = opp.Account.Academic_Calendar__c;
             newDemo.Hidden_Opportunity_Stage__c = opp.StageName;
             insert newDemo;
             
             // set the id of the new Demo created for testing
               newRecordId = newDemo.id;
             
    // create the Opportunity_demo connecting record
            newDemoOpp = New Demo_Opportunity__c();
                  newDemoOPP.Demo__c = newDemo.id;
                  newDemoOPP.Opportunity__c = opp.id;
                  insert newDemoOPP;
                  
                  // create the Contact_demo connecting record
                   // copy over the Contact Roles - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
             List<Demo_Contact__c> Contacts = new List<Demo_Contact__c>();
             for (OpportunityContactRole ocr : [Select c.Id, c.contactid From OpportunityContactRole c where OpportunityID = :opp.id]) {
                  newDemoCon = New Demo_Contact__c();
                  newDemoCon.contact__c = ocr.contactid;
                  newDemoCon.Demo__c = newDemo.id;
                  contacts.add(newDemoCon);
             }
             insert Contacts;
                  
             
               } catch (Exception e){
             // roll everything back in case of error
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
         }
  
        return new PageReference('/'+newDemo.id+'/e?retURL=%2F'+newDemo.id);
        
    }

}