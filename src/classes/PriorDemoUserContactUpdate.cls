@IsTest
private class PriorDemoUserContactUpdate
{
    private static TestMethod void testTrigger(){
        
        //Step 1 : Data Insertion
        Account a=new Account(Name='Test Account',BillingCountry='Peru',ShippingCountry='Peru');
           insert a;
           Contact c = new Contact(FirstName='John',LastName='Doe',Prior_Sapling_User__c=False, Prior_Demo__c=False);
        insert c;
        Opportunity o = new Opportunity(Name='Test Opportunity',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.10);
        insert o;  
        OpportunityContactRole ocr = new OpportunityContactRole (OpportunityID = o.id, ContactID=c.id, role='Decision Maker') ;     
        insert ocr;
        
        test.startTest();

        o.Demo_Status__c = 'Demo No Show';
        o.Student_Subscription__c = 60;
        update o;

        c = [SELECT Id, Prior_Demo__c, Prior_Sapling_User__c FROM Contact WHERE Id=:c.Id];
        system.assertEquals(True, c.Prior_Demo__c);
        system.assertEquals(True, c.Prior_Sapling_User__c);

        test.stopTest();
    }
}