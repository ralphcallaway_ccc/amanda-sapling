@isTest(seeAllData=true)
private class TestOpportunityOnAccountDetailExtension {
 
    static testMethod void testOpportunityOnAccountDetailExtension() {
 
       // setup a reference to the page the controller is expecting with the parameters
        PageReference pref = Page.AccountPage;
        Test.setCurrentPage(pref);
 
        // setup a ship to account
        Account acc = new Account();
        acc.Name = 'PSAV 6FOO';
        acc.Type = 'Supplier';
        insert acc;
        
                  
        // create new opp record
        Opportunity opp = new Opportunity();
        Opp.CloseDate = Date.newInstance(2020,01,01);
        opp.StageName = 'Confirmed Teach/Class Schedule';
        opp.Name = 'Test Opportunity';
        opp.Type = 'New Business';
        opp.Academic_Term__c = 'Spring';
        opp.Subject__c = 'Biochemistry';
        opp.AccountID= acc.ID;
        
        insert opp;
        
        // create second new opp record
        Opportunity opp2 = new Opportunity();
        Opp2.CloseDate = Date.newInstance(2020,12,01);
        opp2.StageName = 'Confirmed Teach/Class Schedule';
        opp2.Name = 'Test Opportunity2';
        opp2.Type = 'New Business';
        opp2.Academic_Term__c = 'Spring';
        opp2.Subject__c = 'General Chemistry';
        opp2.AccountID= acc.ID;
        
        insert opp2;
        
        
           
 
        // Construct the standard controller
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
 
        // create the controller
        OpportunityOnAccountDetailExtension ext = new OpportunityOnAccountDetailExtension(sc);
        
        string OppList = ext.GetselectedSubject();
        
        ext = new opportunityonAccountDetailExtension(sc);
        ext.setselectedsubject('Biochemistry');
        ext.getitems();
        ext.handleaccountselected();
        ext.test();
    }
 
        
 
}