Public with Sharing Class Courseinlineedit {
    Private Final Opportunity opp;
    Private Final Course__c co;
    Public CourseInLineEdit (Apexpages.StandardController Stdcontroller)
    {
        this.opp= (Opportunity)Stdcontroller.getrecord();

        FirstCourses = [select ID, Name,Contact__c,First_Tech_TA_Contact__c, Chapters_ish__c,  Key_Code_Info__c,  Course_Delivered__c ,TA_Notes__c, Adoption_Agreement__c from Course__c where Opportunity__c= :this.opp.id order by Name];
      System.debug('My courses = ' + FirstCourses);
      
    }
    public list<Course__c> FirstCourses{get;set;}

    public pagereference updateListItems()
    {
        if(GlobalHelper.CheckForNotNull(FirstCourses))
        {
            List<database.saveresult> saveResults = database.update(FirstCourses);
        }
        return null;
    }
}