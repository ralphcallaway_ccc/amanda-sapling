@IsTest
private class CannontDeleteOpportunity95
{
    static testMethod void testDelete()
    {
         Account a=new Account(Name='Test Account');
           insert a;
           
           // create test opportunity
        Opportunity o = new Opportunity(Name='Test Opportunity',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.95);
        insert o;
        
            //create test user
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
      User u = new User(Alias = 'system', Email='saplingsystemuser@testorg.com',
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
      LocaleSidKey='en_US', ProfileId = p.Id,
      TimeZoneSidKey='America/Los_Angeles', UserName='saplingsystemuser@testorg.com');


      System.runAs(u) {
      // The following code runs as user 'u'  
      
           delete o;
        }
    } // <-- Here's the missing bracket
static testMethod void testDeleteFail()
    {
            
        
  //create test user
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
      User u1 = new User(Alias = 'standa', Email='saplingstandarduser@testorg.com',
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
      LocaleSidKey='en_US', ProfileId = p.Id,
      TimeZoneSidKey='America/Los_Angeles', UserName='saplingstandarduser@testorg.com');

test.starttest();
      System.runAs(u1) {
      // The following code runs as user 'u1'  
      system.assert(UserInfo.getProfileId().subString(0,15) != '00e600000013jtd');
           try
           {
Account a=new Account(Name='Test Account');
           insert a;
// create test opportunity
        Opportunity o = new Opportunity(Name='Test Opportunity',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule', Probability=95, accountId = a.id);
        insert o;
              delete o;
              // should throw an exception - the following assertion will cause an error if the code carries on
           }
           catch (Exception e)
           {
           system.debug('Error:'+e);

} 
               // expected - could assert the message here
               
           
           
        }
  test.stopTest();
      }
      }