public class OpportunityCloneWithItemsController {
 
    //added an instance variable for the standard controller
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Opportunity opp {get;set;}
    // set the id of the record that is created -- ONLY USED BY THE TEST CLASS
    public ID newRecordId {get;set;}
 
    // initialize the controller
    public OpportunityCloneWithItemsController(ApexPages.StandardController controller) {
 
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        opp = (Opportunity)controller.getRecord();
        
 
    }
 
    // method called from the VF's action attribute to clone the opp
    public PageReference cloneWithItems() {
 
         // setup the save point for rollback
         Savepoint sp = Database.setSavepoint();
         Opportunity newOpp;
 
         try {
 
              //copy the opportunity - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
             opp = [select Id, Name, Type, AccountID, Estimated_of_Students__c, StageName, Description, Hidden_New_Class_Start_Date__c,Competitor__c, Competitors_in_Consideration__c from Opportunity where id = :opp.id];
             newOpp = opp.clone(false);
             newOpp.Academic_Term__c = NULL;
             newOpp.CloseDate = date.Today();
             if (opp.StageName == 'Students Registering') {
                 newOpp.StageName = 'Prior Sapling User';
                 } else {
                 newOpp.StageName = 'Confirmed Teaching/Class Schedule';
            }
            if (opp.StageName == 'Students Registering') {
                         newOpp.Type = 'Renewal';
                         } else {
                         newOpp.Type = NULL;
            }
            newOpp.Hidden_New_Class_Start_Date__c = NULL;
             insert newOpp;
            opp.Hidden_New_Class_Start_Date__c = NULL;
             update Opp;
             
            
            
             // set the id of the new Opp created for testing
               newRecordId = newOpp.id;
 
             // copy over the line items - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
             List<OpportunityLineItem> items = new List<OpportunityLineItem>();
             for (OpportunityLineItem oli : [Select o.Id, o.unitprice, o.Quantity, o.PricebookEntryID, o.Description From OpportunityLineItem o where OpportunityID = :opp.id]) {
                  OpportunityLineItem newoli = oli.clone(false);
                  newoli.OpportunityID = newOpp.id;
                  items.add(newoli);
             }
             insert items;
             
              // copy over the Contact Roles - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
             List<Opportunity_Contact_Role__c> Roles = new List<Opportunity_Contact_Role__c>();
             for (Opportunity_Contact_Role__c ocr : [Select c.Id, c.Contact__c, c.Primary__c, c.Role__c From Opportunity_Contact_Role__c c where Opportunity__c = :opp.id]) {
                  Opportunity_Contact_Role__c newocr = ocr.clone(false);
                  newocr.Opportunity__c = newOpp.id;
                  roles.add(newocr);
             }
             insert Roles;

         } catch (Exception e){
             // roll everything back in case of error
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
         }
  
        return new PageReference('/'+newOpp.id+'/e?retURL=%2F'+newOpp.id);
        
    }

}