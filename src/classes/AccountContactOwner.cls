@IsTest
private class AccountContactOwner
{
    private static TestMethod void testTrigger(){
        
        //Step 1 : Data Insertion
        Account a=new Account(Name='Test Account');
           insert a;
           Contact c = new Contact(Accountid=a.id, FirstName='John',LastName='Doe');
        insert c;
        
        
        test.startTest();
        
        
        //Perform the dml action on which trigger gets fired , like insert, update ,delete , undelete, in your case you have to update account record that you created in above  
        //create test user
            Profile p = [SELECT Id FROM Profile WHERE Name='Custom Standard User'];
            UserRole r = [Select id from userrole where name='Admin/Other'];
      User u1 = new User(Alias = 'standa', Email='saplingstandarduser@testorg.com',
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
      LocaleSidKey='en_US', ProfileId = p.Id, userroleid=r.id,
      TimeZoneSidKey='America/Los_Angeles', UserName='saplingstandarduser@testorg.com');


      System.runAs(u1) {
      // The following code runs as user 'u'  
       // create test contact
        Contact c1 = new Contact(FirstName='Jane',LastName='Doe', Accountid=a.id);
        insert c1;
        
        //assert your results using system.assert and system.asserEquals

//reload objects to make sure values are loaded
        c1 = [select id, name, Account.id, OwnerId from contact where id=:c1.id];
c = [select id, name, Account.id, OwnerId from contact where id=:c.id];

a= [select id, ownerId from Account where id=:a.id];
        system.assert(c1.OwnerID == a.OwnerID);
        
        
        
        test.stopTest();
    }
}
}