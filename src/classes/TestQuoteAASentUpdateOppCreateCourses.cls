@IsTest
private class TestQuoteAASentUpdateOppCreateCourses
{
    private static TestMethod void testTrigger(){
        
        //Step 1 : Data Insertion
        Account a=new Account(Name='Test Account');
           insert a;
           Contact c = new Contact(FirstName='John',LastName='Doe');
        insert c;
        Opportunity o = new Opportunity(Name='Test Opportunity',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.95, Class_End_Date__c=system.today()+30, Course_Sequence__c='1 of 1', Imporant_Information_for_the_Tech_TA__c = 'Test Notes for Tech TA');
        insert o;      
        OpportunityContactRole ocr = new OpportunityContactRole (OpportunityID = o.id, ContactID=c.id, role='Decision Maker') ;     
        insert ocr;  
        Quote q= new Quote (Name='Test Quote', ContactID=c.id, class_end_date__c= system.today()+30, Description='Test Tech TA Notes', Course_Sequence__c = '1 of 1', OpportunityID=o.id);
        insert q;
        
        
        
        try
{
  //Perform the dml action on which trigger gets fired , like insert, update ,delete , undelete  
  Task t= new Task (Subject='Email: Sapling Learning Adoption Agreement', WhoID=c.id, WhatID=q.id);
  insert t;
}catch (Exception e)
{
  System.debug('My error = ' + e);
}
        
    }
}