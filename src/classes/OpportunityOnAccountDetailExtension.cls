global class OpportunityOnAccountDetailExtension implements SObjectPaginatorListener {
 
String selectedSubject;
Account acc;
     
    global List<oppwrapper> accounts   {get;set;}
    global SObjectPaginator paginator  {get;set;} 
    global Integer          pageNumber {get;set;}
    private String sortDirection = 'DESC';
    private String sortExp = 'CloseDate';
     
List<opportunity> oppList = new List<opportunity>();
 
    public OpportunityOnAccountDetailExtension(ApexPages.StandardController controller) {
        this.acc = (Account)controller.getRecord();
        oppList = null;
        this.accounts = new List<oppwrapper>();
        this.paginator = new SObjectPaginator(
            10,                          //pageSize
            new List<integer>{10, 25, 50, 100, 200},  //pageSizeIntegerOptions
            this                        //listener
        ); 
        //this.paginator.setRecords(getoppList());
         
    }
     
    public String getselectedSubject() {
        return selectedSubject;
    }
  
    public void setselectedSubject(String selectedSubject) {
        system.debug('%%%%%%%%%%%% ' + this.selectedSubject);
        this.selectedSubject = selectedSubject;
        system.debug('%%%%%%%%%%%% ' + this.selectedsubject);
    }
     
    public List<selectoption> getItems() {
            List<selectoption> options = new List<selectoption>();
            options.add(new SelectOption('','--SELECT Opportunity Subject--'));
             
            Schema.DescribeFieldResult field = Opportunity.Subject__c.getDescribe();
  
        for (Schema.PicklistEntry f : field.getPicklistValues())
          options.add(new SelectOption(f.getLabel(),f.getLabel()));
           
            return options;
        }
     
    /*    
    public List<opportunity> getoppList(){
        system.debug('$$$$$$$$$$$$$$$$$$ ' + selectedSubject);
            if(selectedSubject==null){
                oppList  = null;
            }else{
            oppList  = [select name, stageName, subject__c Amount, CloseDate from Opportunity where AccountId =: acc.id and subject__c =: String.escapeSingleQuotes(selectedSubject)];
            }
             
            return oppList;
                 
    }
    */
     
   public PageReference handleAccountSelected(){
        system.debug('$$$$$$$$$$ in handleAccountSelected()');
        string sortFullExp = sortExpression  + ' ' + sortDirection;
        string query = 'select name, stageName, Amount, CloseDate, subject__c from Opportunity where AccountId =' ;
        query = query + ' ' + acc.id + ' ' + ' ';
        query = query + 'and Subject__c =' + ' '  + String.escapeSingleQuotes(selectedSubject) + ' '+ ' order by ' + sortFullExp;
        system.debug('################## ' + query);
        this.paginator.setRecords([select name, stagename, Amount, CloseDate, subject__c from Opportunity where AccountId =: acc.id and subject__c =: String.escapeSingleQuotes(selectedSubject) order by CloseDate DESC]);
         
        return null;
    }
     
     
    public PageReference test(){
        return null;
    } 
 
 
    global void handlePageChange(List<sobject> newPage){
        this.accounts.clear();
        if(newPage != null && newPage.size() > 0){
            for(Integer i = 0; i < newPage.size(); i++){
                this.accounts.add(
                    new OppWrapper(
                        (Opportunity)newPage.get(i)
                        , i + this.paginator.pageStartPosition + 1
                    )
                );
            }
        }
    }
     
     
    global PageReference skipToPage(){
        this.paginator.skipToPage(pageNumber);
        return null;
    }
     
       public String sortExpression
   {
     get
     {
        return sortExp;
     }
     set
     {
       //if the column is clicked on then switch between Ascending and Descending modes
       if (value == sortExp)
         sortDirection = (sortDirection == 'DESC')? 'ASC' : 'DESC';
       else
         sortDirection = 'DESC';
       sortExp = value;
     }
   }
 
 public String getSortDirection()
 {
    //if not column is selected 
    if (sortExpression == null || sortExpression == '')
      return 'DESC';
    else
     return sortDirection;
 }
 
 public void setSortDirection(String value)
 {  
   sortDirection = value;
 }
  
     
        
    public class OppWrapper{
         
        public Opportunity obj{get;set;}
        public Integer serialNumber{get;set;}
        public Boolean selected{get;set;}
         
        public OppWrapper(Opportunity obj, Integer serialNumber){
            this.obj = obj;
            this.serialNumber = serialNumber;
            system.debug('@@@@@@@@@@@@@@@@ ' + obj.CloseDate);
        }
 
         
    }
     
     
}