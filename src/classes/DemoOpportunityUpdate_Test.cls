@isTest
private class DemoOpportunityUpdate_Test {

	// Test Methods 

	@isTest
	private static void testDemoUpdateChangesOpportunity() {
		// create some test data
		Account testAccount = TestUtil.createAccount();
		Opportunity testOpportunity = TestUtil.generateOpportunity(testAccount.id);
		testOpportunity.Demo_Status__c = null;
		testOpportunity.Date_of_Demo__c = null;
		insert testOpportunity;
		Demo__c testDemo = TestUtil.generateDemo();
		testDemo.Demo_Status__c = null;
		testDemo.Date_of_Demo__c = null;
		insert testDemo;
		TestUtil.createDemoOpportunity(testDemo.id, testOpportunity.id);

		// validate initial conditions
		assertOppty(testOpportunity.id, null, null);

		// update demo status and demo date
		String demoStatus = TestUtil.TEST_STRING;
		Date demoDate = System.today();
		Test.startTest();
		testDemo.Date_of_Demo__c = demoDate;
		testDemo.Demo_Status__c = demoStatus;
		update testDemo;
		Test.stopTest();

		// validate post conditions
		assertOppty(testOpportunity.id, demoStatus, demoDate);
	}

	// Helper Functions 

	private static void assertOppty(Id opptyId, String demoStatus, Date demoDate) {
		Opportunity testOpportunity = [
			SELECT Date_of_Demo__c, Demo_Status__c 
			FROM Opportunity WHERE Id = :opptyId
		];
		system.assertEquals(demoStatus, testOpportunity.Demo_Status__c);
		system.assertEquals(demoDate, testOpportunity.Date_of_Demo__c);
	}

}
