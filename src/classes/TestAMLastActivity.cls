@IsTest
private class TestAMLastActivity
{
    private static TestMethod void testTrigger(){
        
        //Step 1 : Data Insertion
        Account a=new Account(Name='Test Account');
           insert a;
           Contact c = new Contact(FirstName='John',LastName='Doe');
        insert c;
        Opportunity o = new Opportunity(Name='Test Opportunity',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.95);
        insert o;        
        
        test.startTest();
        
        //Perform the dml action on which trigger gets fired , like insert, update ,delete , undelete, in your case you have to update account record that you created in above  
       Task t=new Task (WhoID=c.id, WhatID=o.id, subject= 'Email: Sapling Learning Adoption Agreement');
       insert t;
        
        test.stopTest();
    }
}