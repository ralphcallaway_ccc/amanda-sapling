public with sharing class TriggerTest_OpptyownerCopy {
 
    static TestMethod void testOpptyownerCopy() {
 
        // Grab two Users
        User[] users = [select Id from User where Isactive= True limit 2];
        User u1 = users[0];
        User u2 = users[1];
 
        // Create an Opportunity
        System.debug('Creating Opportunity');
        Opportunity o1 = new Opportunity(CloseDate = Date.newInstance(2008, 01, 01), Name = 'Test Opportunity', StageName = 'New', OwnerId = u1.Id);
        insert o1;
 
        // Test: Hidden_Opportunity_Owner__c should be set to user 1
        Opportunity o2 = [select id, OwnerId, Hidden_Opportunity_Owner__c from Opportunity where Id = :o1.Id];
        System.assertEquals(u1.Id, o2.OwnerId);
        System.assertEquals(u1.Id, o2.Hidden_Opportunity_Owner__c);
 
        // Modify Owner
        o2.OwnerId = u2.Id;
        update o2;
 
        // Test: Hidden_Opportunity_Owner__c should be set to user 2
        Opportunity o3 = [select id, OwnerId, Hidden_Opportunity_Owner__c from Opportunity where Id = :o2.Id];
        System.assertEquals(u2.Id, o3.OwnerId);
        System.assertEquals(u2.Id, o3.Hidden_Opportunity_Owner__c);
    }
}