Public with Sharing Class ContactDemoinlineedit {
    Private Final Opportunity opp;
    Private Final Opportunity_Contact_Role__c ocr;
    Public ContactDemoInLineEdit (Apexpages.StandardController Stdcontroller)
    {
        this.opp= (Opportunity)Stdcontroller.getrecord();

        FirstContacts = [select ID, Contact__c, Demo_Status__c, Date_of_Demo__c, Opportunity__c,
        date_demo_completed__c, demo_time_zone__c, account_manager_notes__c, sales_scientist_notes__c,
        predemo_info_quality__c, time_demo_requested__c, Sales_Scientist__c, Demo_Tracked_On__c, Marketing_Campaign__c, Contact_Name__c
        from Opportunity_Contact_Role__c where Opportunity__c= :this.opp.id order by Name];
      System.debug('My contacts = ' + FirstContacts);
      
    }
    public list<Opportunity_Contact_Role__c> FirstContacts{get;set;}

    public pagereference updateListItems()
    {
   
    
        if(GlobalHelper.CheckForNotNull(FirstContacts))
        {
            try{
            List<database.saveresult> saveResults = database.update(FirstContacts);
            }
            catch(Exception e){
                    ApexPages.addMessages(e);
            }
            }
        return null;
    }
}