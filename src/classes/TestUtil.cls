/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Place all data creation utility functions in this class so they can be shared
	across multiple tests.  That way if new validation rules break a test you only
	have to make a change in one place.
	
	Also includes helpful functions for generating random data as well as properties
	with special picklist values.
	
Usage:
	Please reference the test methods for usage examples.
*/
@isTest // prevents methods from being called outside test context and excludes code from apex code limits
public class TestUtil {

	/* Test Lock */

	static {
		// Per the apex documentation it shouldn't be possible to call methods in this class outside of
		// test context, but as of 6/2/12 that wasn't being enforced in sandboxes.
		system.assert(Test.isRunningTest(), 'TestUtil methods may only be called from test methods');
	}
	
	/* Quick Test Values */

	public static Decimal DEFAULT_PRICE = 1;
	public static Decimal DEFAULT_QUANTITY = 1;
	public static Date TEST_DATE = System.today();
	public static String TEST_STRING = generateRandomString(16);

	/* Standard Object Functions */

	// Accounts
	public static Account createAccount() {
		Account account = generateAccount();
		insert account;
		return account;
	}
	public static Account generateAccount() {
		// always use long random string for name to avoid issues with de-duping packages
		return new Account(name = generateRandomString(16)); 
	}
	
	/* OPTIONAL - Delete/Uncomment if org has person accounts enabled
	// Person Accounts
	public static Account createPersonAccount() {
		Account personAccount = generatePersonAccount();
		insert personAccount;
		return personAccount;
	}
	public static Account generatePersonAccount() {
		return new Account(
			  lastName = TEST_STRING
			, recordTypeId = PERSON_ACCOUNT_RT_ID
		);
	}
	*/
	
	// Attachments
	public static Attachment createAttachment(Id parentId) {
		Attachment attachment = generateAttachment(parentId);
		insert attachment;
		return attachment;
	}
	public static Attachment generateAttachment(Id parentId) {
		return new Attachment(
			  parentId = parentId
			, name = TEST_STRING
			, body = Blob.valueOf(TEST_STRING)
		);
	}
	
	// Campaigns
	public static Campaign createCampaign() {
		Campaign campaign = generateCampaign();
		insert campaign;
		return campaign;
	} 
	public static Campaign generateCampaign() {
		return new Campaign(
			  name = TEST_STRING
			, isActive = true
		);
	}
	
	// Case
	public static Case createCase() {
		Case testCase = generateCase();
		insert testCase;
		return testCase;
	}
	public static Case generateCase() {
		return new Case(subject = TEST_STRING, description = TEST_STRING);
	}
	
	// Contacts
	public static Contact createContact(Id accountId) {
		Contact contact = generateContact(accountId);
		insert contact;
		return contact;
	}
	public static Contact generateContact(Id accountId) {
		// use long random string for last name to avoid issues with de-duping packages
		return new Contact(lastName = generateRandomString(16), accountId = accountId);
	}
	
	// Events
	public static Event createEvent() {
		Event testEvent = generateEvent();
		insert testEvent;
		return testEvent;
	}
	public static Event generateEvent() {
		return new Event(startDateTime = system.now(), endDateTime = system.now());
	}
	
	// Leads
	public static Lead createLead() {
		Lead lead = generateLead();
		insert lead;
		return lead;
	}
	public static Lead generateLead() {
		return new Lead(lastName = generateRandomString(16), company = generateRandomString(16)); // use long random string for lastName/companyName to avoid issues with de-duping packages
	}
	 
	// Opportunity
	public static Opportunity createOpportunity(Id accountId) {
		Opportunity opportunity = generateOpportunity(accountId);
		insert opportunity;
		return opportunity;
	}
	public static Opportunity generateOpportunity(Id accountId) {
		return new Opportunity(name = TEST_STRING, closeDate = TEST_DATE, stageName = TEST_STRING, accountId = accountId);
	}

	// Opportunity Line Items
	public static OpportunityLineItem createOpportunityLineItem(Id opportunityId, Id pricebookEntryId) {
		OpportunityLineItem lineItem = generateOpportunityLineItem(opportunityId, pricebookEntryId);
		insert lineItem;
		return lineItem;
	}
	public static OpportunityLineItem generateOpportunityLineItem(Id opportunityId, Id pricebookEntryId) {
		return new OpportunityLineItem(
			  opportunityId = opportunityId
			, pricebookEntryId = pricebookEntryId
			, quantity = DEFAULT_QUANTITY
			, totalPrice = DEFAULT_PRICE
		);
	}

	// Pricebook
	public static Pricebook2 createPricebook() {
		Pricebook2 pricebook = generatePricebook();
		insert pricebook;
		return pricebook;
	}
	public static Pricebook2 generatePricebook() {
		return new Pricebook2(
			  name = TEST_STRING
			, isActive = true
		);
	}
	
	// Pricebook Entries
	// fails if seeAllDate=true not set for api version 24+
	public static PricebookEntry createPricebookEntry(Id productId, Id pricebookId) {
		return createPricebookEntries(new Id[] { productId }, pricebookId)[0];
	}
	public static List<PricebookEntry> createPricebookEntries(List<Id> productIds, Id pricebookId) {
		Set<Id> haveStandardPrice = new Set<Id>();
		for(PricebookEntry pbe : [
			select product2Id 
			from PricebookEntry 
			where pricebook2.isStandard = true 
			and product2Id in :productIds 
		]) {
			haveStandardPrice.add(pbe.product2Id);
		}
		List<PricebookEntry> standardPBEs = new List<PricebookEntry>();
		List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
		for(Id productId : productIds) {
			if(!haveStandardPrice.contains(productId)) {
				standardPBEs.add(generatePricebookEntry(productId, STANDARD_PRICEBOOK.id));
			}
			pricebookEntries.add(generatePricebookEntry(productId, pricebookId));
		}
		if(!standardPBEs.isEmpty())
			insert standardPBEs;
		insert pricebookEntries;
		return pricebookEntries;
	}
	private static PricebookEntry generatePricebookEntry(Id productId, Id pricebookId) {
		return new PricebookEntry(
			  product2Id = productId
			, pricebook2Id = pricebookId
			, isActive = true
			, unitPrice = DEFAULT_PRICE
		);
	}	

	// Products
	public static Product2 createProduct() {
		Product2 product = generateProduct();
		insert product;
		return product;
	}
	public static Product2 generateProduct() {
		return new Product2(name = TEST_STRING, isActive = true);
	}

	/* OPTIONAL - Delete/Uncomment if org has quotes enabled
	// Quote
	public static Quote createQuote(Id opportunityId) {
		Quote quote = generateQuote(opportunityId);
		insert quote;
		return quote;
	}
	public static Quote generateQuote(Id opportunityId) {
		return new Quote(name = TEST_STRING, opportunityId = opportunityId);
	} */

	// Task
	public static Task createTask() {
		Task task = generateTask();
		insert task;
		return task;
	}
	public static Task generateTask() {
		return new Task();
	}

	// User
	public static User createUser() {
		User testUser = generateUser();
		insert testUser;
		return testUser;
	}
	public static User generateUser() {
		String testString = generateRandomString(8); // can't be longer than eight so we can use for alias
		String testDomain = generateRandomString(12) + '.com'; // use a long domain to avoid potential duplicate usernames
		String testEmail = generateRandomEmail(testDomain);
		return new User(lastName = testString,
			userName = testEmail,
			profileId = SYSADMIN_PROFILE_ID,
			alias = testString,
			email = testEmail,
			emailEncodingKey = 'ISO-8859-1',
			languageLocaleKey = 'en_US',
			localeSidKey = 'en_US',
			timeZoneSidKey = 'America/Los_Angeles'
		);
	}
	
	/* Custom Objects */
	
	// Demo
	public static Demo__c createDemo() {
		Demo__c testDemo = generateDemo();
		insert testDemo;
		return testDemo;
	}
	public static Demo__c generateDemo() {
		return new Demo__c();
	}

	// Demo Opportunity 
	public static Demo_Opportunity__c createDemoOpportunity(Id demoId, Id opportunityId) {
		Demo_Opportunity__c testDemoOpportunity = generateDemoOpportunity(demoId, opportunityId);
		insert testDemoOpportunity;
		return testDemoOpportunity;
	}
	public static Demo_Opportunity__c generateDemoOpportunity(Id demoId, Id opportunityId) {
		return new Demo_Opportunity__c(Demo__c = demoId, Opportunity__c = opportunityId);
	}
	
	/* Object Process Functions */
	
	// Case Close
	public static Case closeCase(Case record) {
		record.status = CLOSED_CASE_STATUS;
		return record;
	}
	
	// Lead Convert
	public static Database.LeadConvert getLeadConvertObject(Id leadId) {
		Database.LeadConvert convertObject = new Database.Leadconvert();
		convertObject.setLeadId(leadId);
		convertObject.setConvertedStatus(CONVERTED_LEAD_STATUS);
		return convertObject;
	}
	
	// Opportunity Lose
	public static Opportunity closeLoseOpportunity(Opportunity opportunity) {
		opportunity.stageName = OPPTY_CLOSED_LOST_STAGE;
		return opportunity;
	}
	
	// Opportunity Win
	public static Opportunity closeWinOpportunity(Opportunity opportunity) {
		opportunity.stageName = OPPTY_CLOSED_WON_STAGE;
		return opportunity;
	}

	/* Memoized Properties */
	
	public static String CLOSED_CASE_STATUS {
		get {
			if(null == CLOSED_CASE_STATUS) {
				CLOSED_CASE_STATUS = [select masterLabel from CaseStatus where isClosed = true limit 1][0].masterLabel;
			}
			return CLOSED_CASE_STATUS;
		}
		private set;
	}
	
	public static String CONVERTED_LEAD_STATUS {
		get {
			if(null == CONVERTED_LEAD_STATUS) {
	  			CONVERTED_LEAD_STATUS = [select masterLabel from LeadStatus where isConverted = true limit 1][0].masterLabel;
	  		}
	  		return CONVERTED_LEAD_STATUS;
		}
		private set;
	}

	public static String OPPTY_CLOSED_LOST_STAGE {
		get {
			if(null == OPPTY_CLOSED_LOST_STAGE) {
				 OpportunityStage opptyStage = [select masterLabel from OpportunityStage where isWon = false and isClosed = true limit 1];
				 OPPTY_CLOSED_LOST_STAGE = opptyStage.masterLabel;
			}
			return OPPTY_CLOSED_LOST_STAGE;
		}
		private set;
	}

	public static String OPPTY_CLOSED_WON_STAGE
	{
		get {
			if(null == OPPTY_CLOSED_WON_STAGE) {
				OpportunityStage opptyStage = [select masterLabel from OpportunityStage where isWon = true limit 1];
     			OPPTY_CLOSED_WON_STAGE = opptyStage.masterLabel;	
			}
			return OPPTY_CLOSED_WON_STAGE;
		}
		private set;
	}
	
	/* OPTIONAL - Delete/Uncomment if org has person accounts enabled
	public static Id PERSON_ACCOUNT_RT_ID 
	{
		get {
			if(null == PERSON_ACCOUNT_RT_ID) {
				PERSON_ACCOUNT_RT_ID = [
					select id
					from RecordType
					where isPersonType = true
					and SObjectType = 'Account'
					and isActive = true
					limit 1
				][0].id;
			}
			return PERSON_ACCOUNT_RT_ID;
		}
		private set;
	}
	*/
	
	// fails if @seeAllData=true is not specified for api version 24+
	public static Pricebook2 STANDARD_PRICEBOOK {
		get {
			if(STANDARD_PRICEBOOK == null) {
				STANDARD_PRICEBOOK = [select name, isActive from Pricebook2 where isStandard = true];
			}
			return STANDARD_PRICEBOOK;
		}
		private set;
	}

	public static Id SYSADMIN_PROFILE_ID
	{
		get {
			if(null == SYSADMIN_PROFILE_ID) {
				SYSADMIN_PROFILE_ID = [select id from Profile where name = 'System Administrator'][0].id;
			}
			return SYSADMIN_PROFILE_ID;	
		}
		private set;
	}
		
	/* Random Functions */

	private static Set<String> priorRandoms;
	public static String generateRandomString(){return generateRandomString(null);}
	public static String generateRandomString(Integer length){
		if(priorRandoms == null)
			priorRandoms = new Set<String>();

		if(length == null) length = 1+Math.round( Math.random() * 8 );
		String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
		String returnString = '';
		while(returnString.length() < length){
			Integer charpos = Math.round( Math.random() * (characters.length()-1) );
			returnString += characters.substring( charpos , charpos+1 );
		}
		if(priorRandoms.contains(returnString)) {
			return generateRandomString(length);
		} else {
			priorRandoms.add(returnString);
			return returnString;
		}
	}
	
	public static String generateRandomEmail(){return generateRandomEmail(null);}
	public static String generateRandomEmail(String domain){
		if(domain == null || domain == '')
			domain = generateRandomString(8) + '.com';
		return generateRandomString(3) + System.now().getTime() + '@' + domain; // use timestamp to ensure usernames are unique
	}

	public static String generateRandomUrl() {
		return 'http://' + generateRandomString() + '.com'; 
	}
	
	/* Trigger Fix Functions */
	
	// Functions in this section address data dependencies in existing triggers that need
	// to be address when using api version 24+
	
	/* Example fix function
	// Account
	private static Boolean accountTriggersFixed = false;
	private static void fixAccountTriggers() {
		if(!accountTriggersFixed) {
			// SyncAccountToO1 (after insert, after update)
			// Data dependency on OpertiveIntegrationSettings__c having
			// an org default for O1_Account_Eligible_API_Name__c that
			// is a valid account field
			OperativeIntegrationSettings__c setting = OperativeIntegrationSettings__c.getOrgDefaults();
			if(setting.O1_Account_Eligible_API_Name__c == null) {
				setting.O1_Account_Eligible_API_Name__c = 'createdById';
				upsert setting;
			}
			accountTriggersFixed = true;
		}
	}
	*/

	/* Test Functions */

	// Note: this method may need to be broken up to avoid governor limits if org has a lot of triggers
	@isTest
	private static void testStandardObjectCreators() {
		assertIdNotNull(createUser());
		assertIdNotNull(createCase());
		assertIdNotNull(createLead());
		Account testAccount = createAccount();
		assertIdNotNull(testAccount);
		assertIdNotNull(createContact(testAccount.id));
		assertIdNotNull(createOpportunity(testAccount.id));
		assertIdNotNull(createAttachment(testAccount.id));
		assertIdNotNull(createTask());
		assertIdNotNull(createEvent());
		assertIdNotNull(createCampaign());
	}
	
	@isTest
	private static void testCustomObjectCreators() {
		Demo__c testDemo = createDemo();
		assertIdNotNull(testDemo);
		Account testAccount = createAccount();
		Opportunity testOpportunity = createOpportunity(testAccount.id);
		assertIdNotNull(createDemoOpportunity(testDemo.id, testOpportunity.id));
	}
	
	/* OPTIONAL - Delete/Uncomment if org has quotes enabled
	@isTest
	private static void testQuotes() {
		Account testAccount = createAccount();
		Opportunity testOpportunity = createOpportunity(testAccount.id);
		assertIdNotNull(createQuote(testOpportunity.id));
	} */
	
	/* OPTIONAL - Delete/Uncomment if org has person accounts enabled
	@isTest
	private static void testPersonAccounts() {
		assertIdNotNull(createPersonAccount());
		system.assertNotEquals(null, PERSON_ACCOUNT_RT_ID);
	} */

	@isTest(seeAllData=true)
	private static void testProductCreators() {
		assertIdNotNull(STANDARD_PRICEBOOK);
		Pricebook2 pricebook = createPricebook();
		assertIdNotNull(pricebook);
		Product2 product = createProduct();
		assertIdNotNull(product);
		PricebookEntry pricebookEntry = createPricebookEntry(product.id, pricebook.id);
		assertIdNotNull(pricebookEntry);
		Account testAccount = createAccount();
		Opportunity testOppty = generateOpportunity(testAccount.id);
		testOppty.pricebook2Id = pricebook.id;
		insert testOppty;
		assertIdNotNull(createOpportunityLineItem(testOppty.id, pricebookEntry.id));
	}

	@isTest
	private static void testCloseCase() {
		Case testCase = createCase();
		closeCase(testCase);
		update testCase;
		testCase = [select isClosed from Case where id = :testCase.id];
		system.assertEquals(true, testCase.isClosed);
	}

	@isTest
	private static void testLeadConversion() {
		Lead testLead = createLead();
		Database.convertLead(getLeadConvertObject(testLead.id));
		testLead = [select isConverted from Lead where id = :testLead.id];
		system.assertEquals(true, testLead.isConverted);
	}

	@isTest
	private static void testCloseLoseOpportunity() {
		Opportunity testOppty = createOpportunity(createAccount().id);
		closeLoseOpportunity(testOppty);
		update testOppty;
		testOppty = [select isClosed, isWon from Opportunity where id = :testOppty.id];
		system.assertEquals(true, testOppty.isClosed);
		system.assertEquals(false, testOppty.isWon);
	}
	
	@isTest
	private static void testCloseWinOpportunity() {
		Opportunity testOppty = createOpportunity(createAccount().id);
		closeWinOpportunity(testOppty);
		update testOppty;
		testOppty = [select isWon from Opportunity where id = :testOppty.id];
		system.assertEquals(true, testOppty.isWon);
	}

	@isTest
	private static void testRandomFunctions() {
		system.assertNotEquals(null, generateRandomString());
		system.assertNotEquals(null, generateRandomEmail());
		system.assertNotEquals(null, generateRandomUrl());
	}
	
	@isTest
	private static void testRandomDoesNotRepeat() {
		// With 36 potential characters, a single length string will have a 75% chance
		// of recurring after 10 iterations.  To ensure we skip repeated randoms run through
		// 10 iterations adding each to the set and confirm the final set size is 10.  Because
		// this is probabilistic this test will catch a bug only 75% of the time.
		Set<String> randoms = new Set<String>();
		for(Integer i = 0; i < 10; i++) {
			randoms.add(generateRandomString(1));
		}    
		system.assertEquals(10, randoms.size());
	}
	
	@isTest
	private static void testMemoizedFunctions() {
		system.assertNotEquals(null, CLOSED_CASE_STATUS);
		system.assertNotEquals(null, CONVERTED_LEAD_STATUS);
		system.assertNotEquals(null, OPPTY_CLOSED_WON_STAGE);
		system.assertNotEquals(null, SYSADMIN_PROFILE_ID);
	}
	
	/* Test Helpers */
	
	private static void assertIdNotNull(SObject obj) {
		system.assertNotEquals(null, obj.id);
	}

}