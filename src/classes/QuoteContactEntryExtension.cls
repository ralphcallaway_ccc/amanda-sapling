public with sharing class QuoteContactEntryExtension {

    public Quote theQuote {get;set;} { theQuote = new Quote(); }
    public String searchString {get;set;}
    public Quote_Contact__c[] chosencontacts {get;set;} { chosencontacts = new List<Quote_Contact__c>(); }
    public OpportunityContactRole[] availablecontacts {get;set;} { availableContacts = new List<OpportunityContactRole>(); } 
    public Opportunity theOpp {get;set;} { theOpp = new Opportunity();}
    
    public String toSelect {get; set;}
    public String toUnselect {get; set;}
    public Decimal Total {get;set;}
    
    public Boolean overLimit {get;set;}
    
    private Quote_Contact__c[] forDeletion = new Quote_Contact__c[]{};


    public QuoteContactEntryExtension(ApexPages.StandardsetController controller) {

        Id QuoteID = ApexPages.currentPage().getParameters().get('id');

        // Get information about the Quote being worked on
        theQuote = [select Id, OpportunityID, Opportunity.Name from Quote where Id = :QuoteID limit 1];
       
        
        // If demo contacts were previously selected need to put them in the "selected contacts" section to start with
        chosencontacts = [
            select Id, Contact__c, Adoption_Agreement__c
            from Quote_Contact__c 
            where Adoption_Agreement__c = :QuoteID
        ];
        
        theopp = thequote.opportunity;

        updateAvailableList();

    }
    
    // this is the 'action' method on the page
    public PageReference OpportunityCheck(){
        //if there is only one Opportunity we go with it and save the opp
            if(theQuote.OpportunityId != theopp.id){
                try{
                    theQuote.OpportunityId = theopp.id;
                    update(theQuote);
                }
                catch(Exception e){
                    ApexPages.addMessages(e);
                }
            }
            
            return null;
    
    }
       
    
    public void updateAvailableList() {
        
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'select Id, ContactId, OpportunityId from OpportunityContactRole where OpportunityId = \'' + theOpp.Id + '\'';
                
        // note that we are looking for the search string entered by the user in the First Name OR Last Name
        // modify this to search other fields if desired
        if(searchString!=null){
            qString+= ' and (OpportunityContactRole.Contact.FirstName like \'%' + searchString + '%\' or OpportunityContactRole.Contact.LastName like \'%' + searchString + '%\')';
        }
        
        Set<Id> selectedEntries = new Set<Id>();
        for(Quote_Contact__c d:chosencontacts){
            selectedEntries.add(d.contact__c);
        }
        
        if(selectedEntries.size()>0){
            qString += ' and ContactId not in :selectedEntries';
/*
            String tempFilter = ' and Id not in (';
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            qString+= extraFilter;*/
        }
        
        qString+= ' order by OpportunityContactRole.Contact.Name';
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        availablecontacts = database.query(qString);
        
        // We only display up to 100 results... if there are more than we let the user know (see vf page)
        if(availablecontacts.size()==101){
            availablecontacts.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
    }
    
    public void addTochosencontacts(){
    
        // This function runs when a user hits "select" button next to a contact
    
        for(opportunitycontactrole d : availablecontacts){
            if((String)d.Id==toSelect){
                chosencontacts.add(new Quote_Contact__c(Adoption_Agreement__c=theQuote.Id, contact__c=d.contactid));
                break;
            }
        }
        
        updateAvailableList();  
    }
    

    public PageReference removeFromchosencontacts(){
    
        // This function runs when a user hits "remove" on an item in the "Chosen Contacts" section
    
        Integer count = 0;
    
        for(Quote_Contact__c d : chosencontacts){
            if((String)d.contact__c==toUnselect){
            
                if(d.Id!=null)
                    forDeletion.add(d);
            
                chosencontacts.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }

    public PageReference onSave(){
    
        // If previously selected contacts are now removed, we need to delete them
        if(forDeletion.size()>0)
            delete(forDeletion);
    
        // Previously selected contacts may have new information, so we use upsert here
 try{
      if (chosencontacts.size() > 0)        
          upsert(chosencontacts);
       }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
             
        // After save return the user to the Quote
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Quote   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
 
}