@isTest(seeAllData=true)
private class TestOpportunityCloneWithController {
 
    static testMethod void testOpportunityCloneWithController() {
 
       // setup a reference to the page the controller is expecting with the parameters
        PageReference pref = Page.OpportunityClone;
        Test.setCurrentPage(pref);
 
        // setup a ship to account
        Account shipTo = new Account();
        shipTo.Name = 'PSAV 6FOO';
        shipTo.Type = 'Supplier';
        insert shipTo;
        
        //get standard pricebook
        Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];

        
        Pricebook2 pbk1 = new Pricebook2(); 
        pbk1.Name='Test Pricebook Entry 1';
        pbk1.Description='Test Pricebook Entry 1';
        pbk1.isActive=true;
        insert pbk1;
        
   
        Product2 prd1 = new Product2() ;
        prd1.Name='Test Product Entry 1';
        prd1.Description='Test Product Entry 1';
        prd1.productCode = 'ABC';
        prd1.isActive = true;
        insert prd1;


        PricebookEntry pbe1 = new PricebookEntry();
        pbe1.Product2ID=prd1.id;
        pbe1.Pricebook2ID=standardPb.id;
        pbe1.UnitPrice=50;
        pbe1.isActive = true;
        insert pbe1;
            
        // create new opp record
        Opportunity opp = new Opportunity();
        Opp.CloseDate = Date.newInstance(2020,01,01);
        opp.StageName = 'Confirmed Teaching/Class Schedule';
        opp.Name = 'Test Opportunity';
        opp.Type = 'New Business';
        opp.Pricebook2Id = pbe1.Pricebook2Id;
        opp.Academic_Term__c = 'Spring';
        opp.Hidden_New_Class_Start_Date__c = null;
        opp.AccountID= shipto.ID;
        
        insert opp;
 
        // create a line item for the opp
        OpportunityLineItem oli1 = new OpportunityLineItem();
        oli1.Quantity = 2;
        oli1.OpportunityID = opp.id;
        oli1.PricebookentryID = pbe1.id;
        oli1.UnitPrice = 10;
        insert oli1;
        
         // insert contact
        Contact[] cont = new Contact[]
        {
        new Contact(LastName = 'testcontact1'),
        new Contact(LastName = 'testcontact2')
        };    
        insert cont;    
        // insert contact role     
        Opportunity_Contact_Role__c [] cocr = new Opportunity_Contact_Role__c[]
        {
        new Opportunity_Contact_Role__c(Role__c ='Business User',Opportunity__c=Opp.id ,Contact__c = cont[0].id ,primary__c = True),
        new Opportunity_Contact_Role__c(Role__c ='Business User',Opportunity__c=Opp.id ,Contact__c = cont[0].id ,primary__c = False)
        };
        insert cocr;    
 
        // Construct the standard controller
        ApexPages.StandardController con = new ApexPages.StandardController(opp);
 
        // create the controller
        OpportunityCloneWithItemsController ext = new OpportunityCloneWithItemsController(con);
 
        // Switch to test context
        Test.startTest();
 
        // call the cloneWithItems method
        PageReference ref = ext.cloneWithItems();
        // create the matching page reference
        PageReference redir = new PageReference('/'+ext.newRecordId+'/e?retURL=%2F'+ext.newRecordId);
 
        // make sure the user is sent to the correct url
        System.assertEquals(ref.getUrl(),redir.getUrl());
 
        // check that the new opp was created successfully
        Opportunity newOpp = [select id, Type, StageName, CloseDate, Academic_Term__c, Hidden_New_Class_Start_Date__c from Opportunity where id = :ext.newRecordId];
        System.assertNotEquals(newOpp, null);
        System.assertEquals(null, newopp.type);
        System.assertEquals('Confirmed Teaching/Class Schedule', newopp.stagename);
        System.assertEquals(date.today(), newopp.closedate);
        System.assertEquals(null, newopp.Academic_term__c);
        System.assertEquals(null, newopp.Hidden_New_Class_Start_Date__c);
        // check that the line items were created
        List<OpportunityLineItem> newItems = [Select oli.Id From OpportunityLineItem oli where OpportunityID = :newOpp.id];
        System.assertEquals(newItems.size(),1);
        List<Opportunity_Contact_Role__c> newItems2 = [Select cocr.Id From Opportunity_Contact_Role__c cocr where Opportunity__c = :newOpp.id];
        System.assertEquals(newItems2.size(),2);
 
              
        //update opportunity to new data
        
        opp.stagename = 'Students Registering';
        opp.closedate = Date.newInstance(2020,06,01);
        opp.Hidden_New_Class_Start_Date__c = Date.newInstance(2020,09,01);
        integer Month=opp.Hidden_New_Class_Start_Date__c.Month();
        update opp;
        
        // Cause automatic copy via Hidden Class Start Date
        
        Opportunity newOpp2 = [select id, Type, StageName, CloseDate, Academic_Term__c, Hidden_New_Class_Start_Date__c from Opportunity where closedate = :Date.newInstance(2020,09,01)];
        System.assertNotEquals(newOpp2, null);
        System.assertEquals('Renewal', newopp2.type);
        System.assertEquals('Prior Sapling User', newopp2.stagename);
        System.assertEquals(Date.newInstance(2020,09,01), newopp2.closedate);
        System.assertEquals(null, newopp2.Hidden_New_Class_Start_Date__c);
        // check that the line item was created
        List<OpportunityLineItem> newItems3 = [Select oli.Id From OpportunityLineItem oli where OpportunityID = :newOpp2.id];
        System.assertEquals(newItems3.size(),1);
        
        
        // Switch back to runtime context
        Test.stopTest();
        
        
 
    }
 
}