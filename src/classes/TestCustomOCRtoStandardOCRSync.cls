@IsTest
private class TestCustomOCRtoStandardOCRSync
{
    private static TestMethod void testTrigger(){
        
        //Step 1 : Data Insertion
        Account a=new Account(Name='Test Account');
           insert a;
        Contact c = new Contact(FirstName='John',LastName='Doe');
            insert c;
        Contact c2 = new Contact(FirstName='Jane',LastName='Doe');
           insert c2;
        Opportunity o = new Opportunity(Name='Test Opportunity',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.95, Class_End_Date__c=system.today()+30, Course_Sequence__c='1 of 1', Imporant_Information_for_the_Tech_TA__c = 'Test Notes for Tech TA');
        insert o;  
        
        try
{
  //Perform the dml action on which trigger gets fired , like insert, update ,delete , undelete  
      
        Opportunity_Contact_Role__c cocr = new Opportunity_Contact_Role__c (contact__c= c.id, opportunity__c=o.id, role__c='Decision Maker', primary__c=True) ;     
        insert cocr; 
        Opportunity_Contact_Role__c cocr2 = new Opportunity_Contact_Role__c (contact__c= c2.id, opportunity__c=o.id, role__c='Decision Maker', primary__c=False) ;     
        insert cocr2; 
    	cocr2.role__c='Influencer';
    	update cocr2;
    	delete cocr2;
     

}catch (Exception e)
{
  System.debug('My error = ' + e);
}
        
    }
}