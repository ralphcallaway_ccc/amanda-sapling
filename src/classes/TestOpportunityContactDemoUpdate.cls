@IsTest
private class TestOpportunityContactDemoUpdate
{
    private static TestMethod void testTrigger(){
        
        //Step 1 : Data Insertion
        Account a=new Account(Name='Test Account',BillingCountry='Peru',ShippingCountry='Peru');
           insert a;
        Contact c = new Contact(FirstName='John',LastName='Doe',Prior_Sapling_User__c=False, Prior_Demo__c=True);
        insert c;
        Contact c2 = new Contact(FirstName='Jane',LastName='Doe',Prior_Sapling_User__c=False, Prior_Demo__c=False);
        insert c2;
        Opportunity o = new Opportunity(Name='Test Opportunity',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.10);
        insert o;  
        Opportunity_Contact_Role__c cocr = new Opportunity_Contact_Role__c (Opportunity__c = o.id, Contact__c=c.id, role__c='Decision Maker', Primary__c=True) ;     
        insert cocr;
        Opportunity_Contact_Role__c cocr2 = new Opportunity_Contact_Role__c (Opportunity__c = o.id, Contact__c=c2.id, role__c='Influencer') ;     
        insert cocr2;
        
        test.startTest();

        cocr.Demo_Status__c = 'Demo No Show';
        update cocr;
        cocr2.Demo_Status__c = 'Demo Scheduled';
        update cocr2;

        c = [SELECT Id, Prior_Demo__c, Prior_Sapling_User__c FROM Contact WHERE Id=:c.Id];
        system.assertEquals(True, c.Prior_Demo__c);
         c2 = [SELECT Id, Prior_Demo__c, Prior_Sapling_User__c FROM Contact WHERE Id=:c2.Id];
        system.assertEquals(True, c2.Prior_Demo__c);
        

        test.stopTest();
    }
}