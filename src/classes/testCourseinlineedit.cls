public class testCourseinlineedit {

static testMethod void Courseinlineedit_Test()

{

//Test coverage for the courseinlineedit visualforce page

PageReference pageRef = Page.Courseinlineedit;

Test.setCurrentPageReference(pageRef);

Account a=new Account(Name='Test Account');
    insert a;
Contact c = new Contact(FirstName='John',LastName='Doe');
    insert c;
Opportunity o = new Opportunity(Name='Test Opportunity',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.95);
    insert o;      
OpportunityContactRole ocr = new OpportunityContactRole (OpportunityID = o.id, ContactID=c.id, role='Decision Maker') ;     
    insert ocr;  
Quote q= new Quote (Name='Test Quote', ContactID=c.id, OpportunityID=o.id);
    insert q;
Course__c newCS = new Course__c (Adoption_Agreement__c=q.id, Opportunity__c=o.id, Contact__c=c.id);
	insert newCS;

ApexPages.StandardController sc = new ApexPages.standardController(o);

// create an instance of the controller

Courseinlineedit CourseinlineeditCon = new Courseinlineedit(sc);

//try calling methods/properties of the controller in all possible scenarios

// to get the best coverage.
    
    courseInlineEditCon.updateListItems();


}

}