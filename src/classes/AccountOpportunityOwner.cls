@IsTest
private class AccountOpportunityOwner
{
    private static TestMethod void testTrigger(){
        
        //Step 1 : Data Insertion
        Account a=new Account(Name='Test Account');
           insert a;
           Opportunity o = new Opportunity(Accountid=a.id,Name='Test Opportunity',stagename='Confirmed Teaching/Class Schedule',closedate=system.today());
        insert o;
           
        
        test.startTest();
        
        
        //Perform the dml action on which trigger gets fired , like insert, update ,delete , undelete, in your case you have to update account record that you created in above  
        //create test user
            Profile p = [SELECT Id FROM Profile WHERE Name='Custom Standard User'];
            UserRole r = [Select id from userrole where name='Admin/Other'];
      User u1 = new User(Alias = 'standa', Email='saplingstandarduser@testorg.com',
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
      LocaleSidKey='en_US', ProfileId = p.Id, userroleid=r.id,
      TimeZoneSidKey='America/Los_Angeles', UserName='saplingstandarduser@testorg.com');


      System.runAs(u1) {
      // The following code runs as user 'u'  
       // create test opportunity
       Opportunity o1 = new Opportunity(Accountid=a.id,Name='Test Opportunity2',stagename='Confirmed Teaching/Class Schedule',closedate=system.today());
        insert o1;
        
        //assert your results using system.assert and system.asserEquals

//reload objects to make sure values are loaded
        o1 = [select id, name, Account.id, OwnerId from opportunity where id=:o1.id];
o = [select id, name, Account.id, OwnerId from opportunity where id=:o.id];

a= [select id, ownerId from Account where id=:a.id];
        system.assert(o1.OwnerID == a.OwnerID);
        
        
        
        test.stopTest();
    }
}
}