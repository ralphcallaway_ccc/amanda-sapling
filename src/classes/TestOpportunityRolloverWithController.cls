@isTest(seeAllData=true)
private class TestOpportunityRolloverWithController {
 
    static testMethod void testOpportunityRolloverWithController() {
 
       // setup a reference to the page the controller is expecting with the parameters
        //PageReference pref = Page.OpportunityClone;
        //Test.setCurrentPage(pref);
 
        // setup a ship to account
        Account shipTo = new Account();
        shipTo.Name = 'PSAV 6FOO';
        shipTo.Type = 'Supplier';
        insert shipTo;
        
        //get standard pricebook
        Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];

        
        Pricebook2 pbk1 = new Pricebook2(); 
        pbk1.Name='Test Pricebook Entry 1';
        pbk1.Description='Test Pricebook Entry 1';
        pbk1.isActive=true;
        insert pbk1;
        
   
        Product2 prd1 = new Product2() ;
        prd1.Name='Test Product Entry 1';
        prd1.Description='Test Product Entry 1';
        prd1.productCode = 'ABC';
        prd1.isActive = true;
        insert prd1;


        PricebookEntry pbe1 = new PricebookEntry();
        pbe1.Product2ID=prd1.id;
        pbe1.Pricebook2ID=standardPb.id;
        pbe1.UnitPrice=50;
        pbe1.isActive = true;
        insert pbe1;
            
        // create new opp record
        Opportunity opp = new Opportunity();
        Opp.CloseDate = Date.newInstance(2020,01,01);
        opp.StageName = 'Students Registering';
        opp.Name = 'Test Opportunity';
        opp.Type = 'Renewal';
        opp.Pricebook2Id = pbe1.Pricebook2Id;
        opp.Academic_Term__c = 'Spring';
        opp.Hidden_New_Class_Start_Date__c = null;
        opp.AccountID= shipto.ID;
        insert opp;
        
        
        
 
        // create a line item for the opp
        OpportunityLineItem oli1 = new OpportunityLineItem();
        oli1.Quantity = 2;
        oli1.OpportunityID = opp.id;
        oli1.PricebookentryID = pbe1.id;
        oli1.UnitPrice = 10;
        insert oli1;
        
         // insert contact
        Contact[] cont = new Contact[]
        {
        new Contact(LastName = 'testcontact1'),
        new Contact(LastName = 'testcontact2')
        };    
        insert cont;    
        // insert contact role     
        Opportunity_Contact_Role__c [] ocr = new Opportunity_Contact_Role__c[]
        {
        new Opportunity_Contact_Role__c(Role__c ='Business User',Opportunity__c=Opp.id ,Contact__c = cont[0].id ,primary__c = True),
        new Opportunity_Contact_Role__c(Role__c ='Business User',Opportunity__c=Opp.id ,Contact__c = cont[1].id ,primary__c = False)
        };
        insert ocr;    
 
        // Construct the standard controller
        ApexPages.StandardController con = new ApexPages.StandardController(opp);
 
        // create the controller
        OpportunityCloneWithItemsController ext = new OpportunityCloneWithItemsController(con);
 
             
        // Switch to test context
        Test.startTest();
        // Cause automatic copy via Hidden Class Start Date
        opp.Hidden_New_Class_Start_Date__c = Date.Today();
        update opp;
        // call the cloneWithItems method
        PageReference ref = ext.cloneWithItems();
        // create the matching page reference
        PageReference redir = new PageReference('/'+ext.newRecordId+'/e?retURL=%2F'+ext.newRecordId);
        
        Opportunity newOpp2 = [select id, Type, StageName, CloseDate, Academic_Term__c, Hidden_New_Class_Start_Date__c from Opportunity where id = :ext.newRecordId];
        System.assertNotEquals(newOpp2, null);
        System.assertEquals('Renewal', newopp2.type);
        System.assertEquals('Prior Sapling User', newopp2.stagename);
        System.assertEquals(Date.Today(), newopp2.closedate);
        System.assertEquals(null, newopp2.Hidden_New_Class_Start_Date__c);
        // check that the line item was created
        List<OpportunityLineItem> newItems3 = [Select oli.Id From OpportunityLineItem oli where OpportunityID = :newOpp2.id];
        System.assertEquals(newItems3.size(),1);
        List<Opportunity_Contact_Role__c> newItems4 = [Select ocr.Id From Opportunity_Contact_Role__c ocr where Opportunity__c = :newOpp2.id];
        System.assertEquals(newItems4.size(),2);
        
        // Switch back to runtime context
        Test.stopTest();
        
 
    }
 
}