public with sharing class DemoContactEntryExtension {

    
    public Demo__c theDemo {get;set;} { theDemo = new Demo__c();}
    public String searchString {get;set;}
    public Demo_Contact__c[] chosencontacts {get;set;} { chosencontacts = new List<Demo_Contact__c>(); }
    public Opportunity[] relatedOpportunities {get;set;} { relatedOpportunities = new List<Opportunity>(); }
    public Demo_Opportunity__c[]  demoOpportunities {get;set;}  {  demoOpportunities = new List<Demo_Opportunity__c>(); } 
    public Contact[] availablecontacts {get;set;} { availableContacts = new List<Contact>(); }
    
    public String toSelect {get; set;}
    public String toUnselect {get; set;}
    public Decimal Total {get;set;}
    
    public Boolean overLimit {get;set;}
    
    private Demo_Contact__c[] forDeletion = new Demo_Contact__c[]{};
    private Id demoId;

    private ApexPages.StandardController controller;

    public DemoContactEntryExtension(ApexPages.StandardController controller) {
        this.controller = controller;

        // get the id of the demo
        demoId = controller.getId();
        
        // Grab related demo opportunities
        demoOpportunities = [select Id, Opportunity__c, Demo__c from Demo_Opportunity__c where Demo__c = :demoId];
        
        // Query current demo contacts
        chosencontacts = [
            select Id, Contact__c, Demo__c
            from Demo_Contact__c 
            where Demo__c = :demoID
        ];
        
        // Grab associated opportunities
        Set<Id> opptyIds = new Set<Id>();
        for(Demo_Opportunity__c demoOpportunity : demoOpportunities) {
            opptyIds.add(demoOpportunity.Opportunity__c);
        }

        // Query them
        relatedOpportunities = [
            select Id, Name
            from Opportunity 
            where Id in :opptyIds
        ];

        updateAvailableList();
        
        

    }
    
    // this is the 'action' method on the page
    public PageReference OppCheck(){
        //if there is only one Opp we go with it and save the demo
        /*
            if( demoOpportunities.Demo__c != theDemo.Id ){
                try{
                     demoOpportunities.Demo__c=theDemo.Id;
                    update( demoOpportunities);
                }
                catch(Exception e){
                    ApexPages.addMessages(e);
                }
            }*/
            
            return null;
    
    }
    
     
    
    public void updateAvailableList() {
        
        // set up base query, leave a dummy where clause to ease adding addtional criteria
        String qString = 'select Name from Contact where id != null';
        
        //String qString = 'select Id, ContactId, OpportunityId from OpportunityContactRole where OpportunityId in :relatedOpportunities';

        // note that we are looking for the search string entered by the user in the First Name OR Last Name
        // modify this to search other fields if desired
        if(searchString!=null){
            qString+= ' and (firstName like \'%' + searchString + '%\' or LastName like \'%' + searchString + '%\')';
        }
        
        // if any demo contacts are already available exclude them from the available list;
        Set<Id> selectedContactIds = new Set<Id>();
        for(Demo_Contact__c d:chosencontacts){
            selectedContactIds.add(d.contact__c);
        }
        if(selectedContactIds.size()>0){
            qString += ' and Id not in :selectedContactIds';
        }

        // restrict the results to contacts associated with related opportunities
        qString += ' and Id in (select contactId from OpportunityContactRole where opportunityId in :relatedOpportunities)';
        
        // set the order and limit
        qString+= ' order by Contact.Name';
        qString+= ' limit 101';
        

        // if we don't have an related opportunities return nothing
        if(relatedOpportunities.isEmpty()) {
            availablecontacts = new List<Contact>();
        } else {
            availablecontacts = Database.query(qString);
        }
        
        // We only display up to 100 results... if there are more than we let the user know (see vf page)
        if(availablecontacts.size()==101){
            availablecontacts.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
    }
    
    public void addTochosencontacts(){
    
        // This function runs when a user hits "select" button next to a contact
    
        for(Contact contact : availablecontacts){
            if(contact.Id == toSelect){
                chosencontacts.add(new Demo_Contact__c(Demo__c= demoId, contact__c= contact.id));
                break;
            }
        }
        
        updateAvailableList();  
    }
    

    public PageReference removeFromchosencontacts(){
    
        // This function runs when a user hits "remove" on an item in the "Chosen Contacts" section
    
        Integer count = 0;
    
        for(Demo_Contact__c d : chosencontacts){
            if((String)d.contact__c==toUnselect){
            
                if(d.Id!=null)
                    forDeletion.add(d);
            
                chosencontacts.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }

    public PageReference onSave(){
    
        // If previously selected contacts are now removed, we need to delete them
        if(forDeletion.size()>0)
            delete(forDeletion);
        
        // Previously selected contacts may have new information, so we use upsert here
        try{
            if (chosencontacts.size() > 0)        
                upsert(chosencontacts);
            }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
             
        // After save return the user to the Opportunity
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Opportunity   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
 
}