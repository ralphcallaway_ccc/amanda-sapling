@IsTest
private class TestOpportunityModifiedUpdateContactRole
{
    private static TestMethod void testTrigger(){
        
        //Step 1 : Data Insertion
        Account a=new Account(Name='Test Account',BillingCountry='Peru',ShippingCountry='Peru');
           insert a;
        Opportunity o = new Opportunity(Name='Test Opportunity',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.10, Textbook_Author_Title__c='Other', Competitor__c = 'Other', AM_Last_Activity__c= date.today() );
        insert o; 
        Contact c = new Contact(FirstName='Jolly',LastName='Doe',Prior_Sapling_User__c=False, Prior_Demo__c=True);
        insert c;
        Opportunity_Contact_Role__c cocr = new Opportunity_Contact_Role__c (Opportunity__c = o.id, Contact__c=c.id, role__c='Decision Maker', Primary__c=True) ;     
        insert cocr;
        Opportunity o2 = new Opportunity(Name='Test Opportunity 2',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.10, Textbook_Author_Title__c='Other', Competitor__c = 'Other', AM_Last_Activity__c= date.today() );
        insert o2; 
        Contact c2 = new Contact(FirstName='John',LastName='Doe',Prior_Sapling_User__c=False, Prior_Demo__c=True);
        insert c2;
        Opportunity_Contact_Role__c cocr2 = new Opportunity_Contact_Role__c (Opportunity__c = o2.id, Contact__c=c2.id, role__c='Decision Maker', Primary__c=True) ;     
        insert cocr2;
        Opportunity o3 = new Opportunity(Name='Test Opportunity 3',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.10, Textbook_Author_Title__c='Other', Competitor__c = 'Other', AM_Last_Activity__c= date.today() );
        insert o3; 
        Contact c3 = new Contact(FirstName='Jane',LastName='Doe',Prior_Sapling_User__c=False, Prior_Demo__c=True);
        insert c3;
        Opportunity_Contact_Role__c cocr3 = new Opportunity_Contact_Role__c (Opportunity__c = o3.id, Contact__c=c3.id, role__c='Decision Maker', Primary__c=True) ;     
        insert cocr3;
        
        test.startTest();

        o.StageName = 'Left VM/E-Mail/Conversation';
        update o;
        o2.description = 'test';
        update o2;
        
        Task t = new Task(Subject= 'Test Task', ActivityDate= system.today(), whoid=c3.id, whatid=o3.id);
        insert t;   

        cocr = [SELECT Id, Opportunity__c, Opportunity_Stage__c, AM_Activity__c FROM Opportunity_Contact_Role__c WHERE opportunity__c=:o.Id];
        system.assertEquals('Left VM/E-Mail/Conversation', cocr.Opportunity_Stage__c);
        

        test.stopTest();
    }
}