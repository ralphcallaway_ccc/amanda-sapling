public class NewDemofromContactExtension {
    
     //added an instance variable for the standard controller
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Contact con {get;set;}
    // set the id of the record that is created -- ONLY USED BY THE TEST CLASS
    public ID newRecordId {get;set;}
 
    // initialize the controller
    public NewDemofromContactExtension(ApexPages.StandardController controller) {
 
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        con = (Contact)controller.getRecord();
       } 
         // method called from the VF's action attribute to create the New Demo
    public PageReference NewDemo() {
 
         // setup the save point for rollback
         Savepoint sp = Database.setSavepoint();
         Demo__c newDemo;
         Demo_Contact__c newDemoCON;
 
         try {
 
              //get information from opp to go to Demo - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
             con = [select Id, Name, OwnerID, Account.Academic_Calendar__c from Contact where id = :con.id];
             newDemo = New Demo__c();
             newDemo.AM_RM__c = con.OwnerID;
             newDemo.Academic_Calendar__c = con.Account.Academic_Calendar__c;
             insert newDemo;
             
             // set the id of the new Demo created for testing
               newRecordId = newDemo.id;
             
    // create the Opportunity_demo connecting record
            newDemoCON = New Demo_Contact__c();
                  newDemoCON.Demo__c = newDemo.id;
                  newDemoCON.Contact__c = con.id;
                  insert newDemoCON;
             
               } catch (Exception e){
             // roll everything back in case of error
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
         }
  
        return new PageReference('/'+newDemo.id+'/e?retURL=%2F'+newDemo.id);
        
    }

}