public class testContactDemoinlineedit {

static testMethod void ContactDemoinlineedit_Test()

{

//Test coverage for the contactinlineedit visualforce page

PageReference pageRef = Page.ContactDemoinlineedit;

Test.setCurrentPageReference(pageRef);

Account a=new Account(Name='Test Account');
    insert a;
Contact c = new Contact(FirstName='John',LastName='Doe');
    insert c;
Opportunity o = new Opportunity(Name='Test Opportunity',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.95);
    insert o;      
Opportunity_Contact_Role__c cocr = new Opportunity_Contact_Role__c (Opportunity__c = o.id, Contact__c=c.id, role__c='Decision Maker', Primary__c=True) ;     
    insert cocr;  


ApexPages.StandardController sc = new ApexPages.standardController(o);

// create an instance of the controller

ContactDemoinlineedit ContactDemoinlineeditCon = new ContactDemoinlineedit(sc);

//try calling methods/properties of the controller in all possible scenarios

// to get the best coverage.
    
    contactDemoInlineEditCon.updateListItems();


}

}