public with sharing class opportunityDemoContactEntryExtension {

    public Opportunity theOpp {get;set;} { theOpp = new Opportunity(); }
    public String searchString {get;set;}
    public Demo_Contact__c[] chosencontacts {get;set;} { chosencontacts = new List<Demo_Contact__c>(); }
    public OpportunityContactRole[] availablecontacts {get;set;} { availableContacts = new List<OpportunityContactRole>(); }
    public Demo_Opportunity__c theDemoOpp {get;set;}  { theDemoOpp = new Demo_Opportunity__c(); } 
    public Demo__c theDemo {get;set;} { theDemo = new Demo__c();}
    
    public String toSelect {get; set;}
    public String toUnselect {get; set;}
    public Decimal Total {get;set;}
    
    public Boolean overLimit {get;set;}
    
    private Demo_Contact__c[] forDeletion = new Demo_Contact__c[]{};


    public opportunityDemoContactEntryExtension(ApexPages.StandardsetController controller) {

        Id opptyId = ApexPages.currentPage().getParameters().get('id');

        // Get information about the Opportunity being worked on
        theOpp = [select Id, AccountId, Account.Name from Opportunity where Id = :opptyId limit 1];
        
        //show associated Demo Opp
        theDemoOpp = [select Id, Opportunity__c, Demo__c from Demo_Opportunity__c where Opportunity__c = :opptyID];
        
        // If demo contacts were previously selected need to put them in the "selected contacts" section to start with
        chosencontacts = [
            select Id, Contact__c, Demo__c
            from Demo_Contact__c 
            where Demo__c = :theDemoOpp.Demo__c
        ];

        updateAvailableList();

    }
    
    // this is the 'action' method on the page
    public PageReference DemoCheck(){
        //if there is only one Demo we go with it and save the opp
            if(theOpp.Id != theDemoOpp.Opportunity__c){
                try{
                    theOpp.Id = theDemoOpp.Opportunity__c;
                    insert(theDemoOpp);
                }
                catch(Exception e){
                    ApexPages.addMessages(e);
                }
            }
            
            return null;
    
    }
       
    
    public void updateAvailableList() {
        
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'select Id, ContactId, OpportunityId from OpportunityContactRole where OpportunityId = \'' + theOpp.Id + '\'';
                
        // note that we are looking for the search string entered by the user in the First Name OR Last Name
        // modify this to search other fields if desired
        if(searchString!=null){
            qString+= ' and (OpportunityContactRole.Contact.FirstName like \'%' + searchString + '%\' or OpportunityContactRole.Contact.LastName like \'%' + searchString + '%\')';
        }
        
        Set<Id> selectedEntries = new Set<Id>();
        for(Demo_Contact__c d:chosencontacts){
            selectedEntries.add(d.contact__c);
        }
        
        if(selectedEntries.size()>0){
            qString += ' and ContactId not in :selectedEntries';
/*
            String tempFilter = ' and Id not in (';
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            qString+= extraFilter;*/
        }
        
        qString+= ' order by OpportunityContactRole.Contact.Name';
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        availablecontacts = database.query(qString);
        
        // We only display up to 100 results... if there are more than we let the user know (see vf page)
        if(availablecontacts.size()==101){
            availablecontacts.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
    }
    
    public void addTochosencontacts(){
    
        // This function runs when a user hits "select" button next to a contact
    
        for(opportunitycontactrole d : availablecontacts){
            if((String)d.Id==toSelect){
                chosencontacts.add(new Demo_Contact__c(Demo__c=theDemoOpp.Id, contact__c=d.contactid));
                break;
            }
        }
        
        updateAvailableList();  
    }
    

    public PageReference removeFromchosencontacts(){
    
        // This function runs when a user hits "remove" on an item in the "Chosen Contacts" section
    
        Integer count = 0;
    
        for(Demo_Contact__c d : chosencontacts){
            if((String)d.contact__c==toUnselect){
            
                if(d.Id!=null)
                    forDeletion.add(d);
            
                chosencontacts.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }

    public PageReference onSave(){
    
        // If previously selected contacts are now removed, we need to delete them
        if(forDeletion.size()>0)
            delete(forDeletion);
    
        // Previously selected contacts may have new information, so we use upsert here
 try{
      if (chosencontacts.size() > 0)        
          upsert(chosencontacts);
       }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
             
        // After save return the user to the Opportunity
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Opportunity   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
 
}