public class NewAdoptionfromOpportunityExtension {
    
     //added an instance variable for the standard controller
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Opportunity opp {get;set;}
    // set the id of the record that is created -- ONLY USED BY THE TEST CLASS
    public ID newRecordId {get;set;}
 
    // initialize the controller
    public NewAdoptionfromOpportunityExtension(ApexPages.StandardController controller) {
 
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        opp = (Opportunity)controller.getRecord();
       } 
         // method called from the VF's action attribute to create the New Demo
    public PageReference NewAdoption() {
 
         // setup the save point for rollback
         Savepoint sp = Database.setSavepoint();
         Quote NewAdoption;
         Quote_Contact__c NewAdoptionCon;
 
         try {
 
              //get information from opp to go to Adoption Agreement-Quote - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
             opp = [select Id, OwnerID, Name, Class_End_Date__c, Subject__c from Opportunity where id = :opp.id];
             NewAdoption = New Quote();
             NewAdoption.Name = opp.Name;
             NewAdoption.OpportunityID = opp.ID;
             NewAdoption.Class_End_Date__c = opp.Class_End_Date__c;
             if (opp.Subject__c == 'Analytical Chemistry') {
                 newAdoption.Tech_TA__c = 'Analytical';
                 } else if (opp.Subject__c == 'Biochemistry') {
                 newAdoption.Tech_TA__c = 'Biochemistry Team';
                 } else {
                 newAdoption.Tech_TA__c = NULL;
            }
             insert NewAdoption;
             
             // set the id of the new Demo created for testing
               newRecordId = NewAdoption.id;
             
                  
                  // create the Contact_demo connecting record
                   // copy over the Contact Roles - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
             List<Quote_Contact__c> Contacts = new List<Quote_Contact__c>();
             for (OpportunityContactRole ocr : [Select c.Id, c.contactid From OpportunityContactRole c where OpportunityID = :opp.id]) {
                  NewAdoptionCon = New Quote_Contact__c();
                  NewAdoptionCon.contact__c = ocr.contactid;
                  NewAdoptionCon.Adoption_Agreement__c = NewAdoption.id;
                  contacts.add(NewAdoptionCon);
             }
             insert Contacts;
                  
             
               } catch (Exception e){
             // roll everything back in case of error
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
         }
  
        return new PageReference('/'+NewAdoption.id+'/e?retURL=%2F'+NewAdoption.id);
        
    }

}