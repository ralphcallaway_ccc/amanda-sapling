@istest
private class testopportunitycontactEntry {

static testMethod void theTests(){

{

//Create Test Data

Account a=new Account(Name='Test Account');
    insert a;
Contact c = new Contact(FirstName='John',LastName='Doe', AccountID=a.id);
    insert c;
Opportunity o = new Opportunity(Name='Test Opportunity',closedate=system.today(), stagename='Confirmed Teaching/Class Schedule',Probability=0.95, accountID=a.id);
    insert o;      
Opportunity_Contact_Role__c cocr = new Opportunity_Contact_Role__c (Opportunity__c = o.id, Contact__c=c.id, role__c='Decision Maker', Primary__c=True) ;     
    insert cocr;  

     
        ////////////////////////////////////////
        //  test opportunityContactEntry
        ////////////////////////////////////////
        
           // load the page       
        PageReference pageRef = Page.opportunitycontactEntry;
        pageRef.getParameters().put('Id',cocr.Opportunity__c);
        Test.setCurrentPageReference(pageRef);
        
 List<Opportunity> opps = new List<Opportunity>();
    opps.add( [ Select Id, Accountid From Opportunity Where Id = : cocr.Opportunity__c ] );       
        
        opportunityContactEntryExtension oCEE = new OpportunityContactEntryExtension(new ApexPages.StandardSetController( opps ));
        

        // we know that there is at least one line item, so we confirm
        Integer startCount = oCEE.chosencontacts.size();
        system.assert(startCount>0);

        //test search functionality without finding anything
        oCEE.searchString = 'michaelforce is a hip cat';
        oCEE.updateAvailableList();
        system.assert(oCEE.availablecontacts.size()==0);
        
        //test remove from shopping cart
        oCEE.toUnselect = cocr.contact__c;
        oCEE.removeFromchosencontacts();
        system.assert(oCEE.chosencontacts.size()==startCount-1);
        
        //test save and reload extension
        oCEE.onSave();
        oCEE = new OpportunityContactEntryExtension(new ApexPages.StandardSetController(opps));
        system.assert(oCEE.chosencontacts.size()==startCount-1);
        
        // test search again, this time we will find something
        oCEE.searchString = cocr.Contact__r.FirstName;
        oCEE.updateAvailableList();
        system.assert(oCEE.availablecontacts.size()>0);       

        // test add to Shopping Cart function
        oCEE.toSelect = oCEE.availablecontacts[0].Id;
        oCEE.addTochosencontacts();
        system.assert(oCEE.chosencontacts.size()==startCount);
                
        // test save method - WITHOUT role and primary entered and confirm that error message is displayed
        //oCEE.onSave();
        //system.assert(ApexPages.getMessages().size()>0);
        
        // add required info and try save again
        for(Opportunity_Contact_Role__c ocr : oCEE.chosencontacts){
            ocr.role__c = 'Decision Maker';
            ocr.primary__c = True;
        }
        oCEE.onSave();
        
        // query line items to confirm that the save worked
        Opportunity_Contact_Role__c[] cocr2 = [select Id from Opportunity_Contact_Role__c where Opportunity__c = :cocr.Opportunity__c];
        system.assert(cocr2.size()==startCount);
        
        // test on new Opp (no pricebook selected) to make sure redirect is happening
        Opportunity newOpp = new Opportunity(Name='New Opp',stageName='Pipeline',Amount=10,closeDate=System.Today()+30,AccountId=cocr.Opportunity__r.AccountId);
        insert(newOpp);
        oCEE = new OpportunityContactEntryExtension(new ApexPages.StandardSetController(opps));
        
        // final quick check of cancel button
        System.assert(oCEE.onCancel()!=null);
        
        
        ////////////////////////////////////////
        //  test redirect page
        ////////////////////////////////////////
        
        // load the page
        pageRef = Page.OpportunityContactRedirect;
        pageRef.getParameters().put('Id',cocr2[0].Id);
        Test.setCurrentPageReference(pageRef);

        // load the extension and confirm that redirect function returns something
        OpportunityContactRedirectExtension oCRE = new OpportunityContactRedirectExtension(new ApexPages.StandardController(cocr2[0]));
        System.assert(oCRE.redirect()!=null);
     
    }
}
}