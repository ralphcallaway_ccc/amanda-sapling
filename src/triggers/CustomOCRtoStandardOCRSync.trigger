trigger CustomOCRtoStandardOCRSync on Opportunity_Contact_Role__c (after delete, after insert, after undelete, 
after update) {


    // collect related opportunities
    Set<Id> opptyIds = new Set<Id>();
    List<Opportunity_Contact_Role__c> cocrList = (trigger.isDelete) ? trigger.old : trigger.new;
    for(Opportunity_Contact_Role__c cocr : cocrList) {
        opptyIds.add(cocr.opportunity__c);
    }
    
    // grab list of current custom contact roles and map by opportunity
    Map<Id, Map<Id, Opportunity_Contact_Role__c>> cOcrMap = new Map<Id, Map<Id, Opportunity_Contact_Role__c>>();
    for(Opportunity_Contact_Role__c cocr : [
        select contact__c, opportunity__c, role__c, primary__c 
        from Opportunity_Contact_Role__c
        where opportunity__c in :opptyIds
    ]) {
        // initialize map for opportunity if it's the first time we've seen it
        if(!cOcrMap.containsKey(cocr.opportunity__c)) {
            cOcrMap.put(cocr.opportunity__c, new Map<Id, Opportunity_Contact_Role__c>());
        }
        Map<Id, Opportunity_Contact_Role__c> opportunitycontactMap = cOcrMap.get(cocr.opportunity__c);
        
        // map by contact id
        opportunitycontactMap.put(cocr.contact__c, cocr);
    }
    
    // grab list of current standard contact roles and organize
    Map<Id, Map<Id, OpportunityContactRole>> standardOCRMap = new Map<Id, Map<Id, OpportunityContactRole>>();
    for(OpportunityContactRole ocr : [
        select contactId, opportunityId, role, isPrimary 
        from OpportunityContactRole
        where opportunityId in :opptyIds
    ]) {
        // initialize map for opportunity if it's the first time we've seen it
        if(!standardOCRMap.containsKey(ocr.opportunityId)) {
            standardOCRMap.put(ocr.opportunityId, new Map<Id, OpportunityContactRole>());
        }
        Map<Id, OpportunityContactRole> contactMap = standardOCRMap.get(ocr.opportunityId);
        
        // map by contact id
        contactMap.put(ocr.contactId, ocr);
    }

    // find contact roles that need to be deleted
    List<OpportunityContactRole> toBeDeleted = new List<OpportunityContactRole>();
    for(Id opptyId : standardOCRMap.keySet()) {
        for(Id contactId : standardOCRMap.get(opptyId).keySet()) {
            
            // does this exist in the custom version?
            Boolean exists = false;
            if(cOCRMap.containsKey(opptyId)) {
                Map<Id, Opportunity_Contact_Role__c> opportunitycontactMap = cOCRMap.get(opptyId);
                if(!opportunitycontactMap.containsKey(contactId)) {
                    // doesn't existing in the custom table, so we need to delete it
                    toBeDeleted.add(standardOCRMap.get(opptyId).get(contactId));
                }
            }
                
        }
    }
    delete toBeDeleted;
    
 // find new contact roles that need to be created or updated
    List<OpportunityContactRole> toBeUpserted = new List<OpportunityContactRole>();
    for(Id opptyId : cOCRMap.keySet()) {
        for(Opportunity_Contact_Role__c cocr : cOCRMap.get(opptyId).values()) {
            
            // get the existing one if it exists
            OpportunityContactRole existing = new OpportunityContactRole();
            if(standardOCRMap.containsKey(opptyId)) {
                Map<Id, OpportunityContactRole> contactMap = standardOCRMap.get(opptyId);
                if(contactMap.containsKey(cocr.contact__c)){
                    existing = contactMap.get(cocr.contact__c);
                    } else {
                    existing.contactId = cocr.contact__c;
                    existing.opportunityId= opptyId;

                    }
                } else {
                    existing.contactId = cocr.contact__c;
                    existing.opportunityId= opptyId;
                    }
            
            // update the fields
            existing.role = cocr.role__c;
            existing.isPrimary = cocr.primary__c;
            toBeUpserted.add(existing);
            
        }
    }
    If(toBeUpserted.size() >0) {
    upsert toBeUpserted;
    }



}