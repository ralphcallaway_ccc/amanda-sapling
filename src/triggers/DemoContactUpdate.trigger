trigger DemoContactUpdate on Demo__c (after insert, after update) {

    // map tracks records on DemoID

    Map<String, Demo__c> DemoMap = new Map<String, Demo__c>();

    for( Demo__c record : Trigger.new )
    {
        if( record.Demo_Status__c != null)

        {
              DemoMap.put( record.ID, record );            
        }
        
    }

    if( DemoMap.keySet().size() > 0 ) // if there are no eligible Quote records, end
    {
 
 //map to keep track of the Demo contact roles
    map<Id, Demo_Contact__c> democontacts = new map<Id, Demo_Contact__c>();

    List<Contact> lstToUpdate = new List<Contact>();

    //select OpportunityContactRoles for the opportunities with contact role required

    List<Demo_Contact__c> DemoContactRoles = [select Demo__c, Contact__c from Demo_Contact__c where Demo__c in :DemoMap.keySet() ];


    for (Demo_Contact__c ocr : DemoContactRoles) {
        //puts the contact roles in the map with the Opportunity ID as the key
        democontacts.put(ocr.Contact__c,ocr);
    }

        // update Contact Prior Demo field from the matching record
        for( Contact contact : [Select Prior_Demo__c, Id From Contact Where ID IN : democontacts.keySet()])
        {
            contact.Prior_Demo__c = True;

            lstToUpdate.add(contact);
        }

        if(lstToUpdate.size() > 0)

       {

             // Save Prior Demo value to the database
            Database.SaveResult[] results = Database.update( lstToUpdate);
            for( Database.SaveResult result : results )
             if( !result.isSuccess() ) System.debug( '<< '
             + result.getErrors().size() + ' update error(s) on Contacts: ' + result.getErrors()[0].getMessage() );
      }   
}
}