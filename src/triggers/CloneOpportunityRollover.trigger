trigger CloneOpportunityRollover on Opportunity (after Insert, after Update) {
    
    List <Opportunity> opportunities = [select Id, Name, Type, AccountID, Estimated_of_Students__c, StageName, Hidden_New_Class_Start_Date__c,Competitor__c, Competitors_in_Consideration__c, (select Id, unitprice, Quantity, PricebookEntryID, Description From OpportunityLineItems), (Select Id, Contact__c, Primary__c, Role__c From Opportunity_Contact_Roles__r) from Opportunity where ID IN :Trigger.newMap.keySet()];
    
    
    List<Opportunity> opportunitiesToUpdate = new List<Opportunity>{};
    List<Opportunity> newOpportunities = new List<Opportunity>{};
    List<OpportunityLineItem> newOpportunityLineitems = new List<OpportunityLineItem>{};
    List<opportunity_contact_role__c> newOpportunityContactRoles = new List<opportunity_contact_role__c>{};


    for(Opportunity opp : opportunities){

        if (opp.Hidden_New_Class_Start_Date__c != null) {
            Opportunity newOpp;
            
//          try {
            
                newOpp = opp.clone(false);
                if (opp.Hidden_New_Class_Start_Date__c != NULL) {
                    integer Month=opp.Hidden_New_Class_Start_Date__c.Month();
                
                    if ( Month==11 || Month==12 || Month==1 || Month==2 || Month==3 || Month==4) {
                        newOpp.Academic_Term__c = 'Spring';
                    } else if ( Month==5 || Month==6 || Month==7)  {
                        newOpp.Academic_Term__c = 'Summer';
                    } else if ( Month==8 || Month==9 || Month==10)  {
                        newOpp.Academic_Term__c = 'Fall';
                    } 
                }
                if (opp.Hidden_New_Class_Start_Date__c != NULL) {
                    newOpp.CloseDate = opp.Hidden_New_Class_Start_Date__c;
                }    
                if (opp.StageName == 'Students Registering') {
                    newOpp.StageName = 'Prior Sapling User';
                } else {
                    newOpp.StageName = 'Confirmed Teaching/Class Schedule';
                }
                if (opp.StageName == 'Students Registering') {
                    newOpp.Type = 'Renewal';
                } else {
                    newOpp.Type = NULL;
                }
                newOpp.Hidden_New_Class_Start_Date__c = NULL;
                newOpportunities.add(newOpp);
                opp.Hidden_New_Class_Start_Date__c = NULL;
                opportunitiesToUpdate.add(opp);
                            
//          } catch (Exception e){
//              //ApexPages.addMessages(e);
//              opportunitiesToUpdate = new List<Opportunity>{};
//              newOpportunities = new List<Opportunity>{};
//              newOpportunityLineitems = new List<OpportunityLineItem>{};
//              newOpportunityContactRoles = new List<opportunity_contact_role__c>{};
//          }
        }
    }
    
    // setup the save point for rollback
    Savepoint sp = Database.setSavepoint();

//  try {
    
        insert newOpportunities;
        update opportunitiesToUpdate;
        
        Integer size = newOpportunities.size();
        
        for(Integer i = 0; i < size; i++){
            
            Opportunity opp = opportunitiesToUpdate.get(i);
            Opportunity newOpp = newOpportunities.get(i);
            
            List<OpportunityLineItem> lineItems = opp.OpportunityLineItems;
            //System.debug(lineItems.size=' + 'lineItems.size());
            for (OpportunityLineItem oli : lineItems) {
                 OpportunityLineItem newoli = oli.clone(false);
                 newoli.OpportunityID = newOpp.id;
                 newOpportunityLineitems.add(newoli);
            }
            
            List<opportunity_contact_role__c> roles = opp.opportunity_contact_roles__r;
            //System.debug("roles.size=" + roles.size());
            for (opportunity_contact_role__c ocr : roles) {
                 opportunity_contact_role__c newocr = ocr.clone(false);
                 newocr.Opportunity__c = newOpp.id;
                 newOpportunityContactRoles.add(newocr);
            }
        }

        
        insert newOpportunityLineitems;
        insert newopportunitycontactroles;

//  } catch (Exception e){
//      // roll everything back in case of error
//      Database.rollback(sp);
//      //ApexPages.addMessages(e);
//  }
}