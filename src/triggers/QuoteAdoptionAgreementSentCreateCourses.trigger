trigger QuoteAdoptionAgreementSentCreateCourses on Quote (after update) {

  // gather quotes with agreement sent and their related opptyids
  List<Quote> quotesWithAgreementSent = new List<Quote>();
  Set<Id> relatedOpptyIds = new Set<Id>();
  for(Quote quote : Trigger.new) {
    if(quote.Adoption_Agreement_Sent__c == Date.today() & quote.Adoption_Agreement_Sent__c != trigger.oldMap.get(quote.id).Adoption_Agreement_Sent__c) {
      relatedOpptyIds.add(quote.opportunityId);
      quotesWithAgreementSent.add(quote);
    }
  }

  // query contacts related to opportunities and map by oppty id
  Map<Id, Set<Id>> contactsByOpptyMap = new Map<Id, Set<Id>>();
  for(OpportunityContactRole ocr : [
    select opportunityId, contactId
    from OpportunityContactRole
    where opportunityId in :relatedOpptyIds
  ]) {
    if(!contactsByOpptyMap.containsKey(ocr.opportunityId)) {
      contactsByOpptyMap.put(ocr.opportunityId, new Set<Id>());
    }
    contactsByOpptyMap.get(ocr.opportunityId).add(ocr.contactId);
  }

  // create course records
  List<Course__c> courses = new List<Course__c>();
  for(Quote quote : quotesWithAgreementSent) {
    if(contactsByOpptyMap.containsKey(quote.opportunityId)) {
      for(Id contactId : contactsByOpptyMap.get(quote.opportunityId)) {
        courses.add(new Course__c(
            Name = 'Add Course ID'
          ,  contact__c = contactId
          , adoption_agreement__c = quote.id
          , opportunity__c = quote.opportunityId
        ));
      }
    }
  }
  insert courses;
}