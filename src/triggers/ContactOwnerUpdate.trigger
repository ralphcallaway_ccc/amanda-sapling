trigger ContactOwnerUpdate on Contact (before insert,before update) {
// gather all the Account Id's in a set

   Set<Id> setAccountIds = new Set<Id>();

   Contact[] con = Trigger.new;
   for (Contact c:con)

   {
      setAccountIds.add(c.AccountId);

   }

  

   // store each Account Id with it's owner Id in a map, such that AccountId => Account.OwnerId

   Map<Id, Id> mapAccountToOwner = new Map<Id, Id>();

   for(Account a : [Select Id, OwnerId From Account Where Id IN : setAccountIds])

   {

      mapAccountToOwner.put(a.Id, a.OwnerId);
   }
   for (Contact c:con)
    {
       if(c.AccountId !=null)
       {
           c.OwnerID = mapAccountToOwner.get(c.AccountId); // here you are avoiding the query
       }
   }  
}