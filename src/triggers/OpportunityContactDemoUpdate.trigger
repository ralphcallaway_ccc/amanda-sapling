trigger OpportunityContactDemoUpdate on Opportunity_Contact_Role__c (after insert, after update) {
    
    // map tracks records on Custom Opportunity Contact Role ID
    
    Map<String, Opportunity_Contact_Role__c> COCRMap = new Map<String, Opportunity_Contact_Role__c>();
    Map<Id, Contact> contactMap = new Map<Id, Contact>{};
    
    for( Opportunity_Contact_Role__c record : Trigger.new )
    {
        if( record.Demo_Status__c != null)        
        {
            if (record.Demo_Status__c != 'Demo No Show')
            {

contactMap.put(record.Contact__c, new Contact(Id=record.Contact__c, Prior_Demo__c = true));   
system.debug('###True Prior Demo Status after' + contactmap);         
            }
            else if (record.Demo_Status__c == 'Demo No Show' && !contactMap.containsKey(record.Contact__c))

            {
                //I assume if there is at least one record with Show it takes priority over no show...
                contactMap.put(record.Contact__c, new Contact(Id=record.Contact__c, Prior_Demo__c = false));     
system.debug('###False Prior Demo Status after' + contactmap);        
            }
        }        
    }
    
        
    if(!contactMap.isEmpty())    
    {        
        // Save Prior Demo value to the database
        Database.SaveResult[] results = Database.update(contactMap.values());
system.debug('###Prior Demo Status after' + contactMap.values());     
        for( Database.SaveResult result : results )
        if( !result.isSuccess() ) System.debug( '<< '
        + result.getErrors().size() + ' update error(s) on Contacts: ' + result.getErrors()[0].getMessage() );
    }       
}