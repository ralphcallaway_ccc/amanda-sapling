trigger OpportunityOnsiteConversation on Task (after insert) {

    // map tracks records on WhatID

    Map<String, Task> TaskMap = new Map<String, Task>();

    for( Task record : Trigger.new )
    {
        if( record.subject == 'Onsite Conversation')

        {
              TaskMap.put( record.WhatID, record );            
        }
        
    }

    if( TaskMap.keySet().size() > 0 ) // if there are no eligible Task records, end
    {
// Pull all related Opportunities out of the database in one set, finding them based on matching ID
List<Opportunity> Opportunities = [select id, name, Demo_Status__c from opportunity where ID in :TaskMap.keySet() ];

        // update opportunities Playground Demo field from the matching record 
        for( Opportunity opportunity : opportunities)
        {
            Task record = TaskMap.get( opportunity.id ); // grab the correct record from the map
            Opportunity.Demo_Status__c = 'On Campus Conversation'; 
        }

        // Save Demo value to the database
        Database.SaveResult[] results = Database.update( opportunities );
        for( Database.SaveResult result : results )
if( !result.isSuccess() ) System.debug( '<< ' 
+ result.getErrors().size() + ' update error(s) on Opportunities: ' + result.getErrors()[0].getMessage() );
    }   

}