trigger QuoteAddContacts on Quote (after insert) {

  // gather quotes with agreement sent and their related opptyids
  List<Quote> NewQuotes = new List<Quote>();
  Set<Id> relatedOpptyIds = new Set<Id>();
  for(Quote quote : Trigger.new) {
      relatedOpptyIds.add(quote.opportunityId);
      NewQuotes.add(quote);
  }

  // query contacts related to opportunities and map by oppty id
  Map<Id, Set<Id>> contactsByOpptyMap = new Map<Id, Set<Id>>();
  for(OpportunityContactRole ocr : [
    select opportunityId, contactId, role, isprimary
    from OpportunityContactRole
    where opportunityId in :relatedOpptyIds
  ]) {
    if(!contactsByOpptyMap.containsKey(ocr.opportunityId)) {
      contactsByOpptyMap.put(ocr.opportunityId, new Set<Id>());
    }
    contactsByOpptyMap.get(ocr.opportunityId).add(ocr.contactId);
  }

  // create Quote_Contact records
  List<Quote_Contact__c> QuoteContact = new List<Quote_Contact__c>();
  for(Quote quote : NewQuotes) {
    if(contactsByOpptyMap.containsKey(quote.opportunityId)) {
      for(Id contactId : contactsByOpptyMap.get(quote.opportunityId)) {
        QuoteContact.add(new Quote_Contact__c(
            contact__c = contactId, 
            adoption_agreement__c = quote.id));
      }
    }
  }
  insert QuoteContact;
}