trigger OpportunityOwnerUpdate on Opportunity (before insert,before update) {
// gather all the Account Id's in a set

   Set<Id> setAccountIds = new Set<Id>();

   Opportunity[] opp = Trigger.new;
   for (Opportunity o:opp)

   {
      setAccountIds.add(o.AccountId);

   }

  

   // store each Account Id with it's owner Id in a map, such that AccountId => Account.OwnerId

   Map<Id, Id> mapAccountToOwner = new Map<Id, Id>();

   for(Account a : [Select Id, OwnerId From Account Where Id IN : setAccountIds])

   {

      mapAccountToOwner.put(a.Id, a.OwnerId);
   }
   for (Opportunity o:opp)
    {
       if(o.AccountId !=null)
       {
           o.OwnerID = mapAccountToOwner.get(o.AccountId); // here you are avoiding the query
       }
   }  
}