trigger QuoteAdoptionAgreementSent on Task (after insert) {

    // map tracks records on WhatID

    Map<String, Task> TaskMap = new Map<String, Task>();

    for( Task record : Trigger.new )
    {
        if(String.valueOf(record.subject).startsWith('Email: Sapling Learning Adoption Agreement') == TRUE)

        {
              TaskMap.put( record.WhatID, record );            
        }
        
    }

    if( TaskMap.keySet().size() > 0 ) // if there are no eligible Task records, end
    {
// Pull all related Quotes out of the database in one set, finding them based on matching ID
List<Quote> Quotes = [select id, name, Opportunity.ID, Adoption_Agreement_Sent__c from Quote where ID in :TaskMap.keySet() ];

        // update Quote Adoption Agreement Sent field from the matching record 
        for( Quote Quote : Quotes)
        {
            Task record = TaskMap.get( Quote.id ); // grab the correct record from the map
            Quote.Adoption_Agreement_Sent__c =date.today(); 
Quote.Status='Sent';
        }

        // Save Date value to the database
        Database.SaveResult[] results = Database.update( quotes );
        for( Database.SaveResult result : results )
if( !result.isSuccess() ) System.debug( '<< ' 
+ result.getErrors().size() + ' update error(s) on Quotes: ' + result.getErrors()[0].getMessage() );
    }   

}