trigger DemoOpportunityUpdate on Demo__c (after update) {

    // get a map of demos with field changes we need to sync up
    Map<Id, Demo__c> demosWithChanges = new Map<Id, Demo__c>();
    for(Demo__c newDemo : trigger.new) {
        Demo__c oldDemo = trigger.oldMap.get(newDemo.id);

        // check if any of the fields we care about changed
        if(
            newDemo.Demo_Status__c != oldDemo.Demo_Status__c
         || newDemo.Date_of_Demo__c != oldDemo.Date_of_Demo__c // ADD NEW FIELD HERE
        ) {
            demosWithChanges.put(newDemo.id, newDemo);
        }
    }

    // grap the related opportunities
    Set<Id> opptyIds = new Set<Id>();
    for(Demo_Opportunity__c demoOpportunity : [
        SELECT Opportunity__c FROM Demo_Opportunity__c
        WHERE Demo__c in :demosWithChanges.keySet()
    ]) {
        opptyIds.add(demoOpportunity.Opportunity__c);
    }

    // now update the opportunity based on the latest demo opportunity
    List<Opportunity> updatedOpportunities = new List<Opportunity>();
    for(Opportunity opptyWithMostRecentDemo : [
        SELECT Demo_Status__c, Date_of_Demo__c, // AND HERE
            (
                SELECT Demo__r.Demo_Status__c, Demo__r.Date_of_Demo__c // AND HERE
                FROM Demo_Opportunities__r
                ORDER BY Demo__r.Date_of_Demo__c DESC
                LIMIT 1
            )
        FROM Opportunity WHERE Id in :opptyIds
    ]) {
        if(!opptyWithMostRecentDemo.Demo_Opportunities__r.isEmpty()) {
            Demo__c mostRecentDemo = opptyWithMostRecentDemo.Demo_Opportunities__r[0].Demo__r;
            opptyWithMostRecentDemo.Demo_Status__c = mostRecentDemo.Demo_Status__c;
            opptyWithMostRecentDemo.Date_of_Demo__c = mostRecentDemo.Date_of_Demo__c; // AND HERE
            updatedOpportunities.add(opptyWithMostRecentDemo);
        }
    }

    // commit to the database
    update updatedOpportunities;
}