trigger QuoteAdoptionAgreementSentupdateOpp on Quote (after update) {

    // map tracks records on OpportunityID

    Map<String, Quote> QuoteMap = new Map<String, Quote>();

    for( Quote record : Trigger.new )
    {
        if( record.Adoption_Agreement_Sent__c == date.today() & record.Adoption_Agreement_Sent__c != trigger.oldMap.get(record.id).Adoption_Agreement_Sent__c)

        {
              QuoteMap.put( record.OpportunityID, record );            
        }
        
    }

    if( QuoteMap.keySet().size() > 0 ) // if there are no eligible Quote records, end
    {
// Pull all related Opportunities out of the database in one set, finding them based on matching ID
List<Opportunity> Opportunities = [select id, name, StageName, Course_Sequence__c, Class_End_Date__c, Tech_TA__c, Imporant_Information_for_the_Tech_TA__c from Opportunity where ID in :QuoteMap.keySet() ];

        // update Opportunity Stage field from the matching record 
        for( Opportunity Opportunity : Opportunities)
        {
            Quote record = QuoteMap.get( Opportunity.id ); // grab the correct record from the map
            Opportunity.StageName = 'Confirmed Use'; 
            Opportunity.Tech_TA__c = record.Tech_TA__c;
            Opportunity.Class_End_Date__c = record.Class_End_Date__c;
            Opportunity.Course_Sequence__c= record.Course_Sequence__c;
			Opportunity.Imporant_Information_for_the_Tech_TA__c= record.description;            
        }

        // Save Stage value to the database
        Database.SaveResult[] results = Database.update( opportunities );
        for( Database.SaveResult result : results )
if( !result.isSuccess() ) System.debug( '<< ' 
+ result.getErrors().size() + ' update error(s) on Opportunities: ' + result.getErrors()[0].getMessage() );
    }   

}