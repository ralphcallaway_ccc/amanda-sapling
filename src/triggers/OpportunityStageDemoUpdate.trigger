trigger OpportunityStageDemoUpdate on Opportunity( after update ) {

    // map tracks records on Opportunity ID
    public Demo_Opportunity__c theDemoOpp {get;set;}  { theDemoOpp = new Demo_Opportunity__c(); } 
    Map<String, Opportunity> OpportunityMap = new Map<String, Opportunity>(); 

    for( Opportunity record : Trigger.new )
    {
        if( record.stagename != trigger.oldMap.get(record.id).StageName)

        {
              OpportunityMap.put( record.ID, record );            
        }
        
    }

    if( OpportunityMap.keySet().size() > 0 ) // if there are no eligible Opportunity records, end
    {
    
    //Pull all related Demo Opportunity records in one set finding them based on matching ID
    theDemoOpp = [select Id, Opportunity__c, Demo__c from Demo_Opportunity__c where Opportunity__c = : OpportunityMap.keySet()];
// Pull all related Demos out of the database in one set, finding them based on matching ID
List<Demo__c> Demos = [select ID, Hidden_Opportunity_Stage__c from Demo__c where ID = :theDemoOpp.demo__c ];

        // update Demo Stage field from the matching record 
        for(Demo__c demo : demos)
        {
            Opportunity record = OpportunityMap.get( theDemoOpp.opportunity__c ); // grab the correct record from the map
            demo.Hidden_Opportunity_Stage__c =  record.Stagename; 
        }

        // Save Stage value to the database
        Database.SaveResult[] results = Database.update( demos );
        for( Database.SaveResult result : results )
if( !result.isSuccess() ) System.debug( '<< ' 
+ result.getErrors().size() + ' update error(s) on Demos: ' + result.getErrors()[0].getMessage() );
    }   

}