trigger OpportunityDemoContactUpdate on Opportunity (after insert, after update) {

    // map tracks records on OpportunityID

    Map<String, Opportunity> OpportunityMap = new Map<String, Opportunity>();

    for( Opportunity record : Trigger.new )
    {
        if( record.Demo_Status__c != null)

        {
              OpportunityMap.put( record.ID, record );            
        }
        
    }

    if( OpportunityMap.keySet().size() > 0 ) // if there are no eligible Quote records, end
    {
 
 //map to keep track of the opportunity contact roles
    map<Id, OpportunityContactRole> oppycontactroles = new map<Id, OpportunityContactRole>();

    List<Contact> lstToUpdate = new List<Contact>();

    //select OpportunityContactRoles for the opportunities with contact role required

    List<OpportunityContactRole> ContactRoles = [select OpportunityID, ContactID from OpportunityContactRole where OpportunityID in :OpportunityMap.keySet() ];


    for (OpportunityContactRole ocr : ContactRoles) {
        //puts the contact roles in the map with the Opportunity ID as the key
        oppycontactroles.put(ocr.ContactId,ocr);
    }

        // update Contact Prior Demo field from the matching record
        for( Contact contact : [Select Prior_Demo__c, Id From Contact Where ID IN : oppycontactroles.keySet()])
        {
            contact.Prior_Demo__c = True;

            lstToUpdate.add(contact);
        }

        if(lstToUpdate.size() > 0)

       {

             // Save Prior Demo value to the database
            Database.SaveResult[] results = Database.update( lstToUpdate);
            for( Database.SaveResult result : results )
             if( !result.isSuccess() ) System.debug( '<< '
             + result.getErrors().size() + ' update error(s) on Contacts: ' + result.getErrors()[0].getMessage() );
      }   
}
}