trigger OpptyownerCopy on Opportunity (before Insert, before Update) {

    // handle arbitrary number of opps
    for(Opportunity o : Trigger.New){

      // Has Owner changed? 
    if (o.OwnerID != o.Hidden_Opportunity_Owner__c) { 
    o.Hidden_Opportunity_Owner__c = o.OwnerId;
        }
           }}