trigger AccountTimeZoneUpdate on Account (before insert,before update) {
// gather all the Country Name's in a set

   Set<String> setCountryNames = new Set<String>();

   Account[] acc = Trigger.new;
   for (Account a:acc)
   {
    // only include accounts being inserted or where the BillingCountry is changing
        if (
                System.Trigger.isInsert
            || (System.Trigger.isUpdate && a.BillingCountry != System.Trigger.oldMap.get(a.Id).BillingCountry)
           )
        {
      setCountryNames.add(a.BillingCountry);
   }}
   // store each Country Name with it's timezone in a map, such that CountryName =>Country__c.Time_Zone__c
   
   Map<String, String> mapCountrytoTimezone = new Map<String, String>();

   for(Country__c c: [Select Name, TimeZone__c From Country__c Where Name IN : 

setCountryNames])
   {
      mapCountrytoTimezone.put(c.Name, c.TimeZone__c);
   }
   for (Account a:acc)
    if (
                System.Trigger.isInsert
            || (System.Trigger.isUpdate && a.BillingCountry != System.Trigger.oldMap.get(a.Id).BillingCountry)
           )
        {
    {
     if ((a.billingcountry == 'United States' || a.billingcountry == 'US' || a.billingcountry == 'Canada') && (a.billingstate == 'CT'|| a.billingstate == 'DC'|| a.billingstate == 'DE'|| a.billingstate == 'FL'|| a.billingstate == 'GA'|| a.billingstate == 'IN'|| a.billingstate == 'ME'|| a.billingstate == 'MD'|| a.billingstate == 'MA'|| a.billingstate == 'MI'|| a.billingstate == 'NH'|| a.billingstate == 'NJ'|| a.billingstate == 'NY'|| a.billingstate == 'NC'|| a.billingstate == 'OH'|| a.billingstate == 'PA'|| a.billingstate == 'RI'|| a.billingstate == 'SC'|| a.billingstate == 'VT'|| a.billingstate == 'VA'|| a.billingstate == 'WV'|| a.billingstate == 'ON'|| a.billingstate == 'QC'|| a.billingstate == 'NB'|| a.billingstate == 'NS'|| a.billingstate == 'NL'|| a.billingstate == 'PE')) {
    a.TimeZone__c = 'Eastern Time GMT-5';
} else if ((a.billingcountry == 'United States' || a.billingcountry == 'US' || a.billingcountry == 'Canada') && (a.billingstate == 'AL'|| a.billingstate == 'AR'|| a.billingstate == 'IL'|| a.billingstate == 'IA'|| a.billingstate == 'KS'|| a.billingstate == 'KY'|| a.billingstate == 'LA'|| a.billingstate == 'MN'|| a.billingstate == 'MS'|| a.billingstate == 'MO'|| a.billingstate == 'NE'|| a.billingstate == 'ND'|| a.billingstate == 'OK'|| a.billingstate == 'SD'|| a.billingstate == 'TN'|| a.billingstate == 'TX'|| a.billingstate == 'WI'|| a.billingstate == 'SK'|| a.billingstate == 'MB'|| a.billingstate == 'NU' ))  {
     a.TimeZone__c = 'Central Time GMT-6';
} else if ((a.billingcountry == 'United States' || a.billingcountry == 'US' || a.billingcountry == 'Canada') && (a.billingstate == 'CO'|| a.billingstate == 'ID'|| a.billingstate == 'MT'|| a.billingstate == 'NM'|| a.billingstate == 'UT'|| a.billingstate == 'WY'|| a.billingstate == 'AB'|| a.billingstate == 'NT')) {
    a.TimeZone__c = 'Mountain Time GMT-7';
} else if ((a.billingcountry == 'United States' || a.billingcountry == 'US' || a.billingcountry == 'Canada') && (a.billingstate == 'CA'|| a.billingstate == 'NV'|| a.billingstate == 'OR'|| a.billingstate == 'WA'|| a.billingstate == 'BC'|| a.billingstate == 'YT'))  {
     a.TimeZone__c = 'Pacific Time GMT-8';
} else if ((a.billingcountry == 'United States' || a.billingcountry == 'US') && a.billingstate == 'AZ')  {
     a.TimeZone__c = 'Arizona GMT-7'; 
} else if ((a.billingcountry == 'United States' || a.billingcountry == 'US') && a.billingstate == 'AK')  {
     a.TimeZone__c = 'Alaska GMT-9';    
} else if ((a.billingcountry == 'United States' || a.billingcountry == 'US') && a.billingstate == 'HI')  {
     a.TimeZone__c = 'Hawaii GMT-10'; 
} else if (a.BillingCountry !=null)
       {
           a.TimeZone__c = mapCountrytoTimezone.get(a.BillingCountry); // 
} else {
    a.TimeZone__c = null;
}}
   }  
}