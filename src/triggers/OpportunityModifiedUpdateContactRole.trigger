trigger OpportunityModifiedUpdateContactRole on Opportunity( after update ) {

    // map tracks records on Opportunity ID

    Map<String, Opportunity> OpportunityMap = new Map<String, Opportunity>();

    for( Opportunity record : Trigger.new )
    {
        if( record.stagename != trigger.oldMap.get(record.id).StageName || record.AM_Last_Activity__c != trigger.oldMap.get(record.id).AM_Last_Activity__c  || (record.LASTMODIFIEDDATE != trigger.oldMap.get(record.id).lastmodifieddate && record.lastmodifiedby == record.owner))

        {
              OpportunityMap.put( record.ID, record );            
        }
        
    }

    if( OpportunityMap.keySet().size() > 0 ) // if there are no eligible Opportunity records, end
    {
// Pull all related Opportunity Contact Roles out of the database in one set, finding them based on matching ID
List<Opportunity_Contact_Role__c> ContactRoles = [select Opportunity__c, Contact__c, AM_Activity__c, Opportunity_Stage__c from Opportunity_Contact_Role__c where Opportunity__c in :OpportunityMap.keySet() ];

        // update Contact Role Activity field from the matching record 
        for(Opportunity_Contact_Role__c ocr : contactroles)
        {
        Opportunity record = OpportunityMap.get( ocr.opportunity__c ); // grab the correct record from the map
			if( record.AM_Last_Activity__c != trigger.oldMap.get(record.id).AM_Last_Activity__c){
				ocr.AM_Activity__c =  record.AM_Last_Activity__c; 
				}
			else if (record.stagename != trigger.oldMap.get(record.id).StageName){
				ocr.Opportunity_Stage__c =  record.Stagename;
                ocr.AM_Activity__c = record.LastModifiedDate.date();
				}
			else if (record.LASTMODIFIEDDATE != trigger.oldMap.get(record.id).lastmodifieddate && record.lastmodifiedby == record.owner){
				ocr.AM_Activity__c = record.LastModifiedDate.date();
				}
		}

        // Save Stage value to the database
	Database.SaveResult[] results = Database.update( contactroles );
        for( Database.SaveResult result : results )
	if( !result.isSuccess() ) System.debug( '<< ' 
	+ result.getErrors().size() + ' update error(s) on ContactRoles: ' + result.getErrors()[0].getMessage() );
    }   

}