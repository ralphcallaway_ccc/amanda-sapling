//This is provided as a sample to require a contact on opportunities it is provided without warranty and support. 

trigger opportunity_contact_required on Opportunity (before insert, before update) {

    //map to keep track of the contact_required = 1
    Map<String, Opportunity> oppy_contact = new Map<String, Opportunity>();

    //Trigger.new is an array of opportunities 
    //and adds any that have Contact_Required = 1 to the oppy_contact Map
    for (Integer i = 0; i < Trigger.new.size(); i++) {
        System.debug('*****Required? ' + Trigger.new[i].contact_required__c);
        if  (Trigger.new[i].contact_required__c == 1) {
            oppy_contact.put(Trigger.new[i].id,Trigger.new[i]);
        }
    }

    //map to keep track of the opportunity contact roles
    map<Id, OpportunityContactRole> oppycontactroles = new map<Id, OpportunityContactRole>();

    //select OpportunityContactRoles that are primary for the opportunities with contact role required 

    List<OpportunityContactRole> Primaryroles = [select OpportunityId, IsPrimary, Role, Opportunity.RecordtypeID from OpportunityContactRole
        where (OpportunityContactRole.IsPrimary = True and OpportunityContactRole.OpportunityId
        in :oppy_contact.keySet())];
        
    List<OpportunityContactRole> Implementationroles = [select OpportunityId, IsPrimary, Role, Opportunity.RecordtypeID from OpportunityContactRole
        where (OpportunityContactRole.Role = 'Implementation/Training' and OpportunityContactRole.OpportunityId
        in :oppy_contact.keySet())];

    
    for (OpportunityContactRole ocr : Primaryroles) {
        if(ocr.Opportunity.RecordTypeID == '012600000009YQk'){ 
        //puts the contact roles in the map with the Opportunity ID as the key
        oppycontactroles.put(ocr.OpportunityId,ocr);
    }}
    
    for (OpportunityContactRole ocr2 : Implementationroles) {
        if(ocr2.Opportunity.RecordTypeID == '012600000009YQf'){ 
        //puts the contact roles in the map with the Opportunity ID as the key
        oppycontactroles.put(ocr2.OpportunityId,ocr2);
    }}

    // Loop through the opportunities and check if they exists in the contact roles map or contact role isn't required    
    for (Opportunity oppy : system.trigger.new) {
        //system.debug('List oppy Id - '+oppy.id);
        if  (oppy.contact_required__c ==1 && oppy.RecordTypeID == '012600000009YQk' &&  !oppycontactroles.containsKey(oppy.id))
        {
        Id oppyId = oppy.Id;
        System.debug('## oppyId: ' + oppyId);
        String strURL = '<a href="/p/opp/ContactRoleEditUi/e?oppid=' + oppyId + '" target="_blank" title="Opens in new window">here</a>';
        System.debug('## strURL: ' + strURL);
        oppy.addError('No Primary Contact Exists. Please open the Opportunity Contact Roles Related List below and add a primary contact. Insure that all contacts related to this Opportunity are properly added in the Contact Roles with respective roles assigned.'
);       
        }
        else if (oppy.contact_required__c ==1 && oppy.RecordTypeID == '012600000009YQf' &&  !oppycontactroles.containsKey(oppy.id))
        {
        Id oppyId = oppy.Id;
        System.debug('## oppyId: ' + oppyId);
        String strURL = '<a href="/p/opp/ContactRoleEditUi/e?oppid=' + oppyId + '" target="_blank" title="Opens in new window">here</a>';
        System.debug('## strURL: ' + strURL);
        oppy.addError('No Implementation Contact Exists. Please open the Opportunity Contact Roles Related List below and add an Implementation contact. Insure that all contacts related to this Opportunity are properly added in the Contact Roles with respective roles assigned.'
);       
        }
    } //for    
 }